import React, { useMemo, useState, useEffect } from 'react';
//Componentes
import Catalogos from './components/Navbar/Catalogos';
/* import Contacto from './components/Navbar/Contacto';
 */import Inicio from './components/Navbar/Inicio';
import NavbarApp from './components/Navbar/NavbarApp';
import Nosotros from './components/Navbar/Nosotros';
import Registro from './components/Navbar/Registro/Registro.js';
import Pedidos from './components/Navbar/Pedidos';
import DisplayCatalogs from './components/Catalogs/DisplayCatalogs';
import Direccion from './components/Navbar/Direccion/Direccion';
import SuccessPayment from './components/Cart/Payment/SuccessPayment';
//Components for Route
import {
  BrowserRouter as Router,
  Switch,
  Route,
} from "react-router-dom";
import Login from './components/Navbar/Login/Login';
//PDFReader
//import PDFReader from './components/PDF/PDFReader';
//AuthContext
import AuthContext from './context/AuthContext';
import CartContext from './context/CartContext';

import { getProductsCart, addProductCart, countProductsCart, removeProductCart, removeAllCartProducts } from './utils/cart';

import jwtDecode from 'jwt-decode';
import { setToken, getToken, removeToken } from './utils/token';
import PDFReader from './components/PDF/PDFReader';
import Cart from './components/Pages/Cart';


/* Import Toast for Messages */
import { toast, ToastContainer } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';
import ContactPage from './components/Pages/ContactPage';
import ForgotPasswordPage from './components/Pages/ForgotPasswordPage';
import RecoveryPassword from './components/Pages/RecoveryPassword';
import TerminosCondiciones from './components/Pages/TerminosCondiciones';
import PoliticasPrivacidad from './components/Pages/PoliticasPrivacidad';


const App = () => {

  const [auth, setAuth] = useState(undefined)
  const [reloadUser, setReloadUser] = useState(false)
  const [totalProductCart, setTotalProductCart] = useState(0)
  const [reloadCart, setReloadCart] = useState(false)

  useEffect(() => {
    const token = getToken()
    if (token) {
      setAuth({
        token,
        idUser: jwtDecode(token).id
      })
    } else {
      setAuth(null)
    }
    setReloadUser(false)
  }, [reloadUser])

  useEffect(() => {
    setTotalProductCart(countProductsCart());
    setReloadCart(false)
  }, [reloadCart, auth])

  const login = (token) => {
    setToken(token)
    setAuth({
      token,
      idUser: jwtDecode(token).id
    })
  }

  const logout = () => {
    removeToken()
    setAuth(null)
    
  }

  const authData = useMemo(() => ({
    auth,
    login,
    logout,
    setReloadUser
  }), [auth])

  const addProduct = (product) => {
    const token = getToken()
    if (token) {
      addProductCart(product)
      setReloadCart(true)
    } else {
      toast.warn('Para poder agregar el producto al carrito y seguir comprando, debe estar logueado', {
        theme: 'colored'
      })
    }
  }

  const cartData = useMemo(() => ({
    productCart: totalProductCart,
    addProductCart: (product) => addProduct(product),
    getProductsCart: getProductsCart,
    removeProductCart: (product) => removeProduct(product),
    removeAllProductCart: () => removeAllCartProducts(),
  }), [totalProductCart])

  const removeProduct = (product)=>{
    removeProductCart(product);
    setReloadCart(true);
  }

  if (auth === undefined) return null;


  return (
    <div className="page-container">
      <div className="content-wrap">
        <AuthContext.Provider value={authData}>
          <CartContext.Provider value={cartData}>
            <Router>
              <NavbarApp />
              <Switch>
                <Route path="/" component={Inicio} exact />
                <Route path="/contacto" component={ContactPage} />
                <Route path="/nosotros" component={Nosotros} />
                <Route path="/todos" component={Catalogos} />
                <Route path="/registrarse" component={Registro} />
                <Route path="/login" component={Login} />
                <Route path="/pedidos" component={Pedidos} />
                <Route path="/catalogos/:nombre" component={DisplayCatalogs} />
                <Route path="/direccion" component={Direccion} />
                <Route path="/lectura/:nom/:cat" component={PDFReader} />
                <Route path="/carrito" component={Cart} />
                <Route path="/pago-exitoso" component={SuccessPayment} />
                <Route path="/olvido-contrasena" component={ForgotPasswordPage} />
                <Route path="/recuperar-contrasena" component={RecoveryPassword} />
                <Route path="/terminos-y-condiciones" component={TerminosCondiciones}/>
                <Route path="/politicas-de-privacidad" component={PoliticasPrivacidad}/>
              </Switch>
            </Router>
            {/* {     <div className="App">
     <PDFReader /> 
     <div class="input-group w-50 p-3">
  <div class="input-group-prepend">
    <button class="btn btn-outline-light" type="button">Agregar Producto</button>
  </div>
  <input type="text" class="form-control" placeholder="Agregue el ID del producto para agregarlo al carrito" aria-label="" aria-describedby="basic-addon1"/>
</div>
    </div> } */}
            <ToastContainer
              position="top-right"
              autoClose={5000}
              hideProgressBar={false}
              newestOnTop={false}
              closeOnClick
              rtl={false}
              pauseOnFocusLoss
              draggable
              pauseOnHover
            />
          </CartContext.Provider>
        </AuthContext.Provider>
      </div>
    </div>


  )
}

export default App;
import React from 'react';
import { Form, Formik } from 'formik';
import * as Yup from 'yup';
import { Button } from 'react-bootstrap';
import { TextField } from './Textfield';
import { changePasswordReset } from '../Navbar/User';

const RecoveryMyPassword = () => {
    const searchParams = window.location.search;
    const code = searchParams.split('=')[1];

    const validate = Yup.object({
        password: Yup.string()
            .min(6, 'La contraseña debe de ser de al menos 6 dígitos')
            .required('La contraseña es obligatoria'),
        confirmPassword: Yup.string()
            .oneOf([Yup.ref('password'), null], 'No coinciden las contraseñas')
            .required('Debe de confirmar su contraseña')
        
    });

    return (
        <div>
            <div className="d-flex justify-content-center">
                <h2>Cambia tu contraseña</h2>
            </div>
            <Formik
                initialValues = {{
                    code: code,
                    password: '',
                    confirmPassword: ''
                }}
                validationSchema = {validate}
                onSubmit={(values)=>{
                    changePasswordReset(values.code, values.password, values.confirmPassword);
                }}
            >
                {formik =>(
                    <div className="mt-3">
                        <Form>
                            <TextField label="Ingrese la nueva contraseña" name="password" type="password"/>
                            <TextField label="Repita la contraseña" name="confirmPassword" type="password"/>
                            <Button type="submit" variant="secondary" className="mt-3 w-100" >Solicitar</Button>
                        </Form>
                    </div>
                )}
            </Formik>
        </div>
    )
}

export default RecoveryMyPassword

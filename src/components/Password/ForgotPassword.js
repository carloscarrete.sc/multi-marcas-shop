import { Button } from 'react-bootstrap';
import { Form, Formik } from 'formik';
import React from 'react'
import * as Yup from 'yup';
import { TextField } from './Textfield';
import { resetPasswordApi } from '../Navbar/User';
import { toast } from 'react-toastify';

const ForgotPassword = () => {

    const validate = Yup.object({
        email: Yup.string()
                .email('El correo no tiene un formato válido')
                .required('Por favor, introduzca su correo electrónico')
    });

    const resetMyPassword = (val) =>{
         const validateEmailToRestartPassword = Yup.string().email().required();
        if(!validateEmailToRestartPassword.isValidSync(val.email)){
            toast.warning('El correo electrónico no es válido', {
                theme: 'colored'
            })
        }else{
            resetPasswordApi(val.email);
        } 
    }

    return (
        <div>
            <div className="d-flex justify-content-center">
                <h2>¿Olvidaste tu contraseña?</h2>
            </div>
            <div className="d-flex justify-content-center">
                <h5>Para poder restablecer tu contraseña deberas de poner enseguida la dirección
                    de tu correo electrónico para que podamos enviarte un enlace para que puedas
                    restablecer tu contraseña.
                </h5>
            </div>
            <Formik
                initialValues = {{
                    email: ''
                }}
                validationSchema = {validate}
                onSubmit={(values)=>{

                }}
            >
                {formik =>(
                    <div className="mt-3">
                        <Form>
                            <TextField label="Correo electrónico" name="email" type="email"/>
                            <Button type="submit" variant="secondary" className="mt-3 w-100" onClick={()=>resetMyPassword(formik.values)}>Solicitar</Button>
                        </Form>
                    </div>
                )}
            </Formik>
        </div>

    )
}

export default ForgotPassword

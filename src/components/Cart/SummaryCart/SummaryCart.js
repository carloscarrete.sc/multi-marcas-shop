import React, {useEffect, useState} from 'react'
import useCart from '../../../hooks/useCart'
import Table from 'react-bootstrap/Table'
import {Button } from 'react-bootstrap'
import { Link } from 'react-router-dom'


const SummaryCart = ({products, pricingShipping}) => {

    const [totalPrice, settotalPrice] = useState(0)
    const [contactAdministrador, setContactAdministrador] = useState(false)

    const {removeProductCart} = useCart()
    
    useEffect(() => {
        if(products){
            let price = 0;
            for (const product of products) {
                price+=product[0].Precio;
            }
            settotalPrice(price)
            setContactAdministrador(false);
        }
        if(pricingShipping===-1){
            setContactAdministrador(true);
        }
    }, [products, pricingShipping])
    
    const removeProduct = (product) =>{
        removeProductCart(product);
    }
    
    if(!products) return null;
    if(!pricingShipping) return null;




    return (
        <div className="container">
            <div className="summary-cart">
            <div className="title">
            <h1>Mi carrito de Compras</h1>
            </div>
            <div className="data">
                <Table variant="light" striped bordered hover size="sm" responsive>
                    <thead>
                        <tr>
                            <th>Producto</th>
                            <th>Precio</th>
                            <th>Quitar</th>
                        </tr>
                    </thead>
                    <tbody>
                        {products.map(item=>{
                            return(
                                <tr key={item[0].id}>
                                    <td>{item[0].Nombre}</td>
                                    <td>{'$ '+ item[0].Precio +' MXN'}</td>
                                    <td><Button variant="warning" onClick={()=>removeProduct(item[0].SKU)}><i className="far fa-times-circle"></i></Button></td>
                                </tr>
                            )
                        })}
                                 <tr key="shipping">
                                   <td>Costo de envio:</td>
                                   {contactAdministrador?
                                   (
                                    <div>
                                    <h4>Las compras superan los 12 artículos.</h4>
                                    </div>
                                   ):
                                   (
                                       <td>{`$ ${pricingShipping} MXN`}</td> 
                                   )}
                                   <td></td>
                                </tr>
                                <tr key="total">
                                    <td>Total: </td>
                                    {contactAdministrador?
                                    (
                                        <h6>Para poder realizar una cotización, por favor envie la información de los productos que desea comprar a este <Link to='/contacto'>enlace</Link> </h6>
                                    ):
                                    (
                                        <td>{'$ '+(totalPrice+pricingShipping).toFixed(2) +' MXN'}</td>
                                    )}
                                    <td></td>
                                </tr> 
                    </tbody>
                </Table>
            </div>
        </div>
        </div>
    )
}

export default SummaryCart

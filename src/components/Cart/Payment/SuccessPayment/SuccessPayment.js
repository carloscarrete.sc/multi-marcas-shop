import React, {useState, useEffect} from 'react'
import { useHistory } from 'react-router-dom'
import PaymentSuccessImage from '../../../../assets/images/payment_success.png'

const SuccessPayment = () => {
    const history = useHistory();
    const [timer, setTimer] = useState(10)
    //setTimeout(function(){ history.push('/pedidos') }, 5000);
    useEffect(() => {
        if(timer>0){
            setTimeout(()=>setTimer(timer-1),1000)
        }else{
            history.push('/pedidos')
        }
    })

    return (
        <div>
            <div>
            <div className="d-flex justify-content-center">
                <h1>El pago se ha realizado con éxito</h1>
            </div>
            <div className="d-flex justify-content-center">
            <img src={PaymentSuccessImage} alt="Pago realizado con éxito" className="img-fluid w-50"></img>
        </div>
            <div className="d-flex justify-content-center">
                <h3>Sera redigirido a la página de pedidos en {timer} segundos...</h3>
            </div>
        </div>
        </div>
    )
}

export default SuccessPayment

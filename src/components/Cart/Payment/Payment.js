import React from 'react';
import {Elements} from "@stripe/react-stripe-js";
import {loadStripe} from '@stripe/stripe-js';
import { STRIPE_TOKEN } from '../../../utils/constants';
import FormPayment from './FormPayment';


const stripePromise = loadStripe(STRIPE_TOKEN);

const Payment = ({address, products, pricingShipping}) => {
    return (
        <div>
            <div className="container">
            <h3>Formulario de pago</h3>
            <Elements stripe={stripePromise}>
            <FormPayment address={address}  products={products} pricingShipping={pricingShipping}/>
            </Elements>

            </div>
        </div>
    )
}

export default Payment

import React, { useState, useEffect } from 'react';
import { CardElement, useStripe, useElements } from '@stripe/react-stripe-js';
import { toast } from 'react-toastify';
import useAuth from '../../../../hooks/useAuth';
import useCart from '../../../../hooks/useCart';
import { Button } from 'react-bootstrap';
import Spinner from 'react-bootstrap/Spinner'

import { useHistory } from 'react-router';
 

import './style.css'
import { paymentCardApi } from '../../../../utils/cart';
import { getUserData } from '../../../Navbar/User';

const FormPayment = ({ products, address, pricingShipping }) => {

    const [loading, setLoading] = useState(false)
    const [toOrdersPage, setToOrdersPage] = useState(false)
    const [toRemoveCart, setToRemoveCart] = useState(false)
    const stripe = useStripe()
    const elements = useElements()
    const history = useHistory()

    const {auth, logout} = useAuth()
    const {removeAllProductCart} = useCart()

    const [user, setUser] = useState(undefined)

    const handleSubmit = async (e) => {
        e.preventDefault();
        setLoading(true)

        if (!stripe || !elements) return;

        const cardElement = elements.getElement(CardElement);
        const result = await stripe.createToken(cardElement);

        if (result.error) {
            toast.error(result.error.message, {
                theme: 'colored'
            })
        } else { 
            const response = await paymentCardApi(
                result.token,
                products,
                auth.idUser,
                user.email,
                address,
                pricingShipping,
                logout
            );

            if(response.length>0){
                toast.success('Pedido realizado',{
                    theme: 'colored'
                })
                //removeAllProductCart();
                //history.push('/pedidos')
                setToOrdersPage(true)
                setToRemoveCart(true)

            }else{
                toast.error('Error al realizar el pedido',{
                    theme: 'colored'
                })
            }
        }



        setLoading(false)
    }

    useEffect(() => {

        (async ()=>{
            const getUserDataFromFetch = await getUserData(logout);
            setUser(getUserDataFromFetch)
            if(toOrdersPage && toRemoveCart){
                history.push('/pago-exitoso')
                removeAllProductCart()
            }
        })()
    }, [history, toOrdersPage, removeAllProductCart, toRemoveCart, logout])


    return (
        <div>
            <form className="form-payment" onSubmit={handleSubmit}>
                <CardElement />
                {/* <Button variant="dark" className="mb-3 w-100" type="submit" loading={loading} disabled={!stripe}>Pagar</Button> */}
                <Button variant="dark" disabled={loading?true:false} type="submit" className="mb-3 w-100">
                    {loading&&(
                        <Spinner
                            as="span"
                            animation="grow"
                            size="sm"
                            role="status"
                            aria-hidden="true"
                        />
                    )}
                    Pagar
                </Button>
            </form>

        </div>
    )
}

export default FormPayment

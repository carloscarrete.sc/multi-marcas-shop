import React, { useState, useEffect } from 'react'
import { getAdress } from '../../../utils/address'
import useAuth from '../../../hooks/useAuth'
import { Link, useHistory } from 'react-router-dom'
import Card from 'react-bootstrap/Card'
import './styleAddress.css'

const AndressShipping = ({ setAddress }) => {

    const [addresses, setAddresses] = useState(null)
    const [addressActive, setAddressActive] = useState(null)
    const { auth, logout } = useAuth()

    const [userActive, setUserActive] = useState(undefined)
    const history = useHistory();

    useEffect(() => {
        (async () => {
            if (auth !== null) {
                const response = await getAdress(auth.idUser, logout)
                setAddresses(response || [])
            }else{
                setUserActive(true);
            }
        })()
    }, [auth, logout])

    const changeAddress = (e) => {
        setAddressActive(e.id)
        setAddress(e)
    }

    if (userActive) return null;
    if (!userActive && !auth){
        history.push('/');
        return null;
    }
    if (!addresses) return null;

    return (
        <div className="container">
            {addresses.length === 0 ?
                <div>
                    <h5>No tiene direcciones <Link to='/direccion'>añade tu primer dirección</Link> </h5>
                </div> :
                <div>
                    <div className="row">
                        {addresses.map((item) =>
                        (
                            <div className="col-md-4 col-xs-6 p-3" key={item.id} onClick={() => changeAddress(item)}>
                                <Card border="dark" bg="light" className={item.id === addressActive && "custom-btn"}>
                                    <Card.Header>{item.titulo}</Card.Header>
                                    <Card.Body>
                                        <Card.Title>{item.nombre}</Card.Title>
                                        <Card.Text className="data">

                                            {item.direccion}
                                            <br />
                                            {item.ciudad}, {item.estado}, {item.codigoPostal}
                                            <br />
                                            {'Teléfono: ' + item.telefono}

                                        </Card.Text>
                                    </Card.Body>
                                </Card>
                            </div>
                        ))}
                    </div>
                </div>}
        </div>
    )
}

export default AndressShipping

import React from 'react'

const PoliticasPrivacidad = () => {
    return (
        <div className="container mt-3 mb-5">
            <h2>
                Aviso de privacidad integral
            </h2>
            <h5>
                Multi-Marcas mejor conocido como Multi-Marcas, con domicilio en Alberto Terrones #300 Benites Sur, Durango, Dgo, CP 34000 y portal de internet https://multi-marcas.com, es el responsable del uso y protección de sus datos personales, y al respecto le informamos lo siguiente:
            </h5>
            <h5>
                <strong>
                    ¿Para qué fines utilizaremos sus datos personales?
                </strong>
            </h5>
            <h5>
                Los datos personales que recabamos de usted, los utilizaremos para las siguientes finalidades que son necesarias para el servicio que solicita:

                Respuesta a mensajes del formulario de contacto, Prestación de cualquier servicio solicitado, Envío de productos adquiridos en esta tienda en línea
            </h5>
            <h5>
                <strong>
                    ¿Qué datos personales utilizaremos para estos fines?
                </strong>
            </h5>
            <h5>
                Para llevar a cabo las finalidades descritas en el presente aviso de privacidad, utilizaremos los siguientes datos personales:
            </h5>
            <h5>
                -Datos de identificación y contacto
            </h5>
            <h5>
                <strong>
                    ¿Cómo puede acceder, rectificar o cancelar sus datos personales, u oponerse a su uso o ejercer la revocación de consentimiento?
                </strong>
            </h5>
            <h5>
                Usted tiene derecho a conocer qué datos personales tenemos de usted, para qué los utilizamos y las condiciones del uso que les damos (Acceso). Asimismo, es su derecho solicitar la corrección de su información personal en caso de que esté desactualizada, sea inexacta o incompleta (Rectificación); que la eliminemos de nuestros registros o bases de datos cuando considere que la misma no está siendo utilizada adecuadamente (Cancelación); así como oponerse al uso de sus datos personales para fines específicos (Oposición). Estos derechos se conocen como derechos ARCO.

                Para el ejercicio de cualquiera de los derechos ARCO, debe enviar una petición vía correo electrónico a soporte@multi-marcas.com y deberá contener:
            </h5>
            <h5>
                <ul>
                    <li>Nombre completo del titular</li>
                    <li>Domicilio</li>
                    <li>Teléfono</li>
                    <li>Correo electrónico usado en esta página web</li>
                    <li>Copia de identificación oficial adjunta</li>
                    <li>Asunto «Derechos ARCO»</li>
                </ul>
                Descripción el objeto del escrito, los cuales pueden ser de manera enunciativa más no limitativa los siguientes: Revocación del consentimiento para tratar sus datos personales; y/o Notificación del uso indebido del tratamiento de sus datos personales; y/o Ejercitar sus Derechos ARCO, con una descripción clara y precisa de los datos a Acceder, Rectificar, Cancelar o bien, Oponerse. En caso de Rectificación de datos personales, deberá indicar la modificación exacta y anexar la documentación soporte; es importante en caso de revocación del consentimiento, que tenga en cuenta que no en todos los casos podremos atender su solicitud o concluir el uso de forma inmediata, ya que es posible que por alguna obligación legal requiramos seguir tratando sus datos personales. Asimismo, usted deberá considerar que para ciertos fines, la revocación de su consentimiento implicará que no le podamos seguir prestando el servicio que nos solicitó, o la conclusión de su relación con nosotros.
            </h5>
            <h5>
                ¿En cuántos días le daremos respuesta a su solicitud?
            </h5>
            <h5>
                En 5 días
            </h5>
            <h5>
                d) ¿Por qué medio le comunicaremos la respuesta a su solicitud?
            </h5>
            <h5>
                Al mismo correo electrónico de donde se envío la petición.
            </h5>
            <h5>
                <strong>
                    El uso de tecnologías de rastreo en nuestro portal de internet
                </strong>
            </h5>
            <h5>
                Le informamos que en nuestra página de internet utilizamos cookies, web beacons u otras tecnologías, a través de las cuales es posible monitorear su comportamiento como usuario de internet, así como brindarle un mejor servicio y experiencia al navegar en nuestra página. Los datos personales que obtenemos de estas tecnologías de rastreo son los siguientes:

                Identificadores, nombre de usuario y contraseñas de sesión, Tipo de navegador del usuario, Tipo de sistema operativo del usuario, Fecha y hora del inicio y final de una sesión de un usuario

                Estas cookies, web beacons y otras tecnologías pueden ser deshabilitadas. Para conocer cómo hacerlo, consulte el menú de ayuda de su navegador. Tenga en cuenta que, en caso de desactivar las cookies, es posible que no pueda acceder a ciertas funciones personalizadas en nuestros sitio web.
            </h5>
            <h5>
                <strong>
                    ¿Cómo puede conocer los cambios en este aviso de privacidad?
                </strong>
            </h5>
            <h5>
                El presente aviso de privacidad puede sufrir modificaciones, cambios o actualizaciones derivadas de nuevos requerimientos legales; de nuestras propias necesidades por los productos o servicios que ofrecemos; de nuestras prácticas de privacidad; de cambios en nuestro modelo de negocio, o por otras causas. Nos comprometemos a mantener actualizado este aviso de privacidad sobre los cambios que pueda sufrir y siempre podrá consultar las actualizaciones que existan en el sitio web https://multi-marcas.com.

                Última actualización de este aviso de privacidad: 17/01/2022
            </h5>
        </div>
    )
}

export default PoliticasPrivacidad

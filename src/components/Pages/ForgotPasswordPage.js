import React from 'react'
import ForgotPassword from '../Password/'
import ImagePassword from '../../assets/images/forgot-password.png'

const ForgotPasswordPage = () => {
    return (
        <div className="container mt-3">
            <div className="row">
                <div className="col-md-7">
                    <img src={ImagePassword} alt="Contraseña olvidada" className="img-fluid w-100"/>
                </div>
                <div className="col-md-5 my-auto">
                    <ForgotPassword />
                </div>
            </div>
        </div>
    )
}

export default ForgotPasswordPage

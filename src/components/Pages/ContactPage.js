import React, {useEffect} from 'react'
import Contact from '../Contact'

import MailBox from '../../assets/images/mailbox.png'

import Aos from 'aos'
import { Link } from 'react-router-dom'


const ContactPage = () => {

    useEffect(() => {
        Aos.init({ duration: 2000 });
    }, [])


    return (
        <div className="container mt-3 "> 
            <div className="row">
                <div className="col-md-6 order-1 order-md-0"  data-aos="fade-up">
                    <Contact />
                    <div className="mb-5">
                        <h6>Al enviar este formulario aceptas <Link to="/terminos-y-condiciones">los términos y condiciones </Link> 
                        y las <Link to="/politicas-de-privacidad">políticas de privacidad</Link> de la tienda en línea multi-marcas</h6>
                    </div>
                </div>
                <div className="col-md-6 order-0 order-md-1">
                <img src={MailBox} alt="MailBox" className="img-fluid w-100"  data-aos="fade-left"></img>
            
                </div>
            </div>
        </div>
    )
}

export default ContactPage

import React, {useState, useEffect} from 'react';
import useCart from '../../hooks/useCart';
import SummaryCart from '../Cart/SummaryCart';
import {getProductByName} from '../../utils/product'
import AddressShipping from '../Cart/AddressShipping';

import CartImage from '../../assets/images/cart.png'
import Payment from '../Cart/Payment';

import EmptyCartImage from '../../assets/images/empty_cart.png'
import useAuth from '../../hooks/useAuth';

import Aos from 'aos'


const Cart = () => {

    useEffect(() => {
        Aos.init({ duration: 2000 });
    }, [])


    const {getProductsCart} = useCart();
    const products = getProductsCart();
    const {auth} = useAuth();

    return !products||auth===null ? <EmptyCart /> : <FullCart products={products} />
}

function EmptyCart(){
    return(
        <div data-aos="fade-up">
            <div className="d-flex justify-content-center">
                <h2>El carrito esta vacio</h2>
            </div>
            <div className="d-flex justify-content-center">
            <img src={EmptyCartImage} alt="Carro Vacio" className="img-fluid w-50"></img>
        </div>
        </div>
        
        )
    }
    
    function FullCart(props){
        const [productsData, setProductsData] = useState(null)
        const [address, setAddress] = useState(null)
        const [pricingShipping, setPricingShipping] = useState(0)
        useEffect(() => {
            (async ()=>{
                const productsTemp = [];
                for await(const product of props.products){
                    const data = await getProductByName(product)
                    productsTemp.push(data);
                }
                setProductsData(productsTemp)
                if(!productsTemp!==null){
                    if(productsTemp?.length>0 && productsTemp?.length<4){
                        setPricingShipping(150)
                    }else if(productsTemp?.length>3 && productsTemp?.length<8){
                        setPricingShipping(200)
                    }else if(productsTemp?.length>7 && productsTemp?.length<12){
                        setPricingShipping(280)
                    }else if(productsTemp?.length>11){
                        setPricingShipping(-1)
                    }
                }
            })()
        }, [props.products])
    return(
        <div data-aos="fade-up">
            <div className="row">
                <div className="col-md-4 ">
                    <img src={CartImage} className="img-fluid w-100" alt="Carro de compras"></img>
                    
                </div>
                <div className="col-md-8">
            <SummaryCart products={productsData} pricingShipping={pricingShipping}/>
            <AddressShipping setAddress={setAddress}/>
            {address&& <Payment products={productsData} address={address} pricingShipping={pricingShipping}/>}
                </div>
            </div>
        </div>
    )
}

export default Cart

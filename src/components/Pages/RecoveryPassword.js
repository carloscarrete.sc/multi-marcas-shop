import React from 'react'
import RecoveryMyPassword from '../RecoveryPassword'
import ChangePasswordImage from '../../assets/images/change_password.png'

const RecoveryPassword = () => {
    return (
        <div className="container mt-3">
            <div className="row">
                <div className="col-md-4">
                    <RecoveryMyPassword />
                </div>
                <div className="col-md-8">
                    <img src={ChangePasswordImage} alt="Cambiar contraseña" className="img-fluid w-100"/>
                </div>
            </div>
        </div>
    )
}

export default RecoveryPassword

import axios from 'axios'
import React, {useState, useEffect} from 'react'
import { useParams } from 'react-router'
import Cards from './Cards'

import { BASE_PATH } from '../../utils/constants'

const DisplayCatalogs = () => {

    const [catalogos, setCatalogos] = useState([])  
    const params = useParams()
    const nombre = params.nombre;
    useEffect(()=>{
        const getCatalogs = async()=>{
            try{
                const getActualCatalogs = await axios.get(`${BASE_PATH}/catalogos-disponibles?_Nombre=${nombre}`)
                setCatalogos(getActualCatalogs.data[0].catalogos)
            }catch(error){
                console.log(error)
            }
        }
        getCatalogs()
    },[nombre])

    

    return (
        <div>
            {catalogos.length!==0?
            (
                <Cards displayCatalogs={catalogos} nombreCatalogoFiltro={nombre.toUpperCase()} />
            ):
            (
                <Cards displayCatalogs="no" />
            )  }
        </div>
    )
}

export default DisplayCatalogs

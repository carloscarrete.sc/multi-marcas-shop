import React, {useEffect} from 'react'
import Aos from 'aos'
import { BASE_PATH } from '../../utils/constants';

function Card({nombre,descripcion, imagen}) {

    useEffect(()=>{
        Aos.init({duration:2000});
   },[])

    return (
        <div data-aos="fade"  className="card text-center rounded-3">
            
            <img src= {BASE_PATH+imagen} alt={nombre} className="img-fluid rounded-3 img-thumbnail"/>
            <div className="card-body">
                <h4 className="card-title">{nombre}</h4>
                <p className="card-text text-secondary">
                {descripcion}
                </p>
            </div>
        </div>
    )
}

export default Card

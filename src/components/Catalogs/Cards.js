import React from 'react';
import Card from './Card'
import { useHistory } from 'react-router';


function Cards(props) {
    const history = useHistory();

    return (
        <>  
        {/* La siguiente validación valida si la información que se esta recibiendo
        son los catálogos que estan disponibles o el contenido de los mismos */}
            {props.catalogos?(
                <div className="container p-3">
                <div className="row">
                    {props.catalogos.map((item) => (
                        <div className="col-md-3 col-xs-6 p-3" key={item.id} onClick={()=>history.push(`/catalogos/${item.Nombre}`)}>
                            
                            <Card nombre={item.Nombre} imagen={item.Imagen.url} descripcion={item.Descripcion} />
                        </div>
                    ))}
                </div>
            </div>
            ):(

                <div>
                    {/* Esta es una validación de prop del contenido de los catálogos disponibles
                    donde se valida si tiene contenido, en dado caso de que no se tenga contenido
                    se estara imprimiendo un mensaje mostrando que no esta disponible */}
                 {props.displayCatalogs!=="no"?
                    (
                        <div className="container p-3">
                <div className="row">
                    {props.displayCatalogs.map((item) => (
                        <div className="col-md-3 col-xs-6 p-3" key={item.id} onClick={()=>history.push(`/lectura/${props.nombreCatalogoFiltro}/${item.CatalogoPDF[0]?.hash}`)}>
                            <Card nombre={item.Titulo} imagen={item.Portada.url} descripcion={item.Descripcion} pdf={item.CatalogoPDF[0]?.url}/>
                        </div>
                    ))}
                </div>
            </div>
                    ):
                    (
                        <div className="d-flex justify-content-center mt-3">
                            <h1>No se han encontrado catálogos</h1>
                        </div>
                    )}
                </div>
            )}
        </>
    )
}

export default Cards

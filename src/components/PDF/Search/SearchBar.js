import React, {useState} from 'react'
import './SearchBar.css'
import useCart from '../../../hooks/useCart'
import Button from 'react-bootstrap/Button'

import { useHistory } from 'react-router'

const SearchBar = ({placeholder, data, breakpoint}) => {

    const [filteredData, setFilteredData] = useState([])
    const [wordEntered, setWordEntered] = useState("")
    const [sentSKU, setSentSKU] = useState("")

    const {addProductCart} = useCart();

    const history = useHistory();

    const handleFilter = (e)=>{
        const searchWord = e.target.value;
        setWordEntered(searchWord)
        const newFilter = data.filter((value)=>{
            return value.SKU.toLowerCase().includes(searchWord.toLowerCase());
        })

        if(searchWord===""){
            setFilteredData([])
        }else{
            setFilteredData(newFilter);
        }
    }

    const handleSeachClicked = (value) =>{
        const productToAdd = value.SKU;
        //const changeValue = productToAdd.replace(',','$') */ /* Esto solo se usara si los productos llevan una coma en su nombre */
        setFilteredData([])
        setWordEntered(value.SKU + ' - ' +value.Nombre) 
        setSentSKU(productToAdd)
        //addProductCart(productToAdd)
        //toast.success('El producto ha sido añadido al carrito', {theme: "colored"})
    }

    const addMyProduct = () =>{
        addProductCart(sentSKU)
    }

    return (
        <div className="mb-3">
            <div className="search">
            <div className="searchInputs">
                <input
                    className={breakpoint==='sm'?`w-100 searcher-product-pdf`:`searcher-product-pdf`}
                    onChange={handleFilter}
                    type="text"
                    value={wordEntered}
                    placeholder={placeholder}
                />
                  <Button variant="success" className="addProductToCart ml-3" onClick={()=>addMyProduct()}><i className="fas fa-plus-circle"></i>{breakpoint==='lg'&& 'Añadir al carrito'}</Button>{' '}
                  <Button variant="danger" className="goToCart ml-3" onClick={()=>history.push('/carrito')}><i className="fas fa-shopping-cart"></i>{breakpoint==='lg'&& 'Ir al carrito'}</Button>{' '}

            </div>
            {filteredData!==0 && (
            <div className="dataResult">
                {filteredData.slice(0,5).map((value,key)=>{
                    return <div onClick={()=>handleSeachClicked(value)} className="dataItem" key={key} ><p>{value.SKU +' - ' +value.Nombre}</p></div>
                })}
            </div>
            )
            }
        </div>
        </div>
        
    )
}

export default SearchBar

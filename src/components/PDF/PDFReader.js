import React, { useState, useEffect, useRef } from 'react';
import Loader from './Loader';
import { Document, Page, pdfjs } from 'react-pdf';
import ControlPanel from './ControlPanel';
import SearchBar from './Search/SearchBar';
import {getProducts} from '../../utils/products'

import Joyride from 'react-joyride';
import { useParams } from 'react-router-dom';

import {BASE_PATH} from '../../utils/constants'



pdfjs.GlobalWorkerOptions.workerSrc = `//cdnjs.cloudflare.com/ajax/libs/pdf.js/${pdfjs.version}/pdf.worker.js`;

const PDFReader = () => {
  const [scale, setScale] = useState(1.0);
  const [numPages, setNumPages] = useState(null);
  const [pageNumber, setPageNumber] = useState(1);
  const [isLoading, setIsLoading] = useState(true);
  const [state, setState] = useState(null)

  /* Se establece los productos para posteriormente ser mandandos mediante props a el
  buscador. */
  const [products, setProducts] = useState([])
  const [breakpoint, setBreakpoint] = useState('xs')
  const params = useParams();
  const url = `${BASE_PATH}/uploads/${params.cat}.pdf`
  const catalogoActual = params.nom;
  const documentWrapperRef = useRef()

  useEffect(() => {
    function getScreenSize (){
      const { innerWidth } = window;
      if (innerWidth >= 768) {
        setBreakpoint('lg')
      } else{
        setBreakpoint('sm')
      }
    }
    getScreenSize()
    
    function handleResize() {
      //console.log('resized to: ', window.innerWidth, 'x', window.innerHeight)
      getScreenSize()
  } 

    (async ()=>{
      const response = await getProducts(catalogoActual);
      setProducts(response.data);
      setState({
        run: false,
        setps : [
          {
            target: '.App',
            content: 'Bienvenido a el Visualizador de catálogos de Multi-Marcas, en dónde podrás visualizar los productos de tus catálogos favoritos',
            title: 'MultiMarcas - PDF Reader',
            disableBeacon: true,
          },
          {
            target: '.App',
            content: 'A continuación se te presentará una pequeña guía explicando como puedes comprar el producto de tu elección.',
            title: 'MultiMarcas - PDF Reader',
            disableBeacon: true,
          },
          {
            target: '.control-panel',
            content: 'Este es el panel del lector PDF de Multi-Marcas, en donde se te presentán varias opciones para ver el catálogo en formato PDF',
            title: 'MultiMarcas - PDF Reader',
            disableBeacon: true,
          },
          {
            target: '.firstButton',
            content: 'Este botón te mandará a la primer página del catálogo.',
            title: 'MultiMarcas - PDF Reader',
            disableBeacon: true,
          },
          {
            target: '.secondButton',
            content: 'Este botón te mandará a una página anterior del catálogo.',
            title: 'MultiMarcas - PDF Reader',
            disableBeacon: true,
          },
          {
            target: '.pageNumberPDF',
            content: 'Este cuadro de texto indica cual es el número de página actual del catálogo.',
            title: 'MultiMarcas - PDF Reader',
            disableBeacon: true,
          },
          {
            target: '.lastPageButton',
            content: 'Este botón te mandará a la última página del catálogo.',
            title: 'MultiMarcas - PDF Reader',
            disableBeacon: true,
          },
          {
            target: '.nextPageButton',
            content: 'Este botón te mandará a la siguiente página del catálogo.',
            title: 'MultiMarcas - PDF Reader',
            disableBeacon: true,
          },
          {
            target: '.magnifyingGlassOut',
            content: 'Este botón hará que el catálogo se vea más pequeño (Zoom out).',
            title: 'MultiMarcas - PDF Reader',
            disableBeacon: true,
          },
          {
            target: '.magnifyingGlassIn',
            content: 'Este botón hará que el catálogo se vea más grande (Zoom in).',
            title: 'MultiMarcas - PDF Reader',
            disableBeacon: true,
          },
          {
            target: '.downloadPDF',
            content: 'Aquí podrás descargar el catálogo en formato PDF.',
            title: 'MultiMarcas - PDF Reader',
            disableBeacon: true,
          },
          {
            target: '.printPDF',
            content: 'Con este botón puedes mandar a imprimir alguna página del catálogo',
            title: 'MultiMarcas - PDF Reader',
            disableBeacon: true,
          },
          {
            target: '.selectedDocument',
            content: 'Este es el documento del catálogo en sí. Aquí podrás visualizar los productos del catálogo de tu elección.',
            title: 'MultiMarcas - PDF Reader',
            disableBeacon: true,
          },
          {
            target: '.searcher-product-pdf',
            content: 'En este cuadro de texto puedes buscar por código o nombre el producto de tu elección y hacer click sobre el para posteriormente seleccionarlo.',
            title: 'MultiMarcas - PDF Reader',
            disableBeacon: true,
          },
          {
            target: '.addProductToCart',
            content: 'En este botón puedes agregar el producto a tu carrito de compras una vez que lo hayas buscado y seleccionado en el cuadro de texto.',
            title: 'MultiMarcas - PDF Reader',
            disableBeacon: true,
          }
          ,
          {
            target: '.goToCart',
            content: 'Finalmente, con este botón podrás ir a tu carrito de compras para que veas los productos que fueron añadidos.',
            title: 'MultiMarcas - PDF Reader',
            disableBeacon: true,
          }
        ]
      })
    })()
    window.addEventListener('resize',handleResize)
    
    return _ => {
      window.removeEventListener('resize', handleResize)
    
}
  },[catalogoActual])
  


  function onDocumentLoadSuccess({ numPages }) {
    setNumPages(numPages);
    setIsLoading(false);
  }

  return (
    <div>
      <Loader isLoading={isLoading} />
      <section
        id="pdf-section"
        className="App d-flex flex-column align-items-center w-100"
      >
        <ControlPanel
          breakpoint={breakpoint}
          scale={scale}
          setScale={setScale}
          numPages={numPages}
          pageNumber={pageNumber}
          setPageNumber={setPageNumber}
          file={url}
        />
        <div ref={documentWrapperRef}>
        {/* {console.log(documentWrapperRef.current?.getBoundingClientRect().width)} */}
        <Document
          renderMode="canvas"
          file={url}
          onLoadSuccess={onDocumentLoadSuccess}
          className="selectedDocument"
        >
          <Page pageNumber={pageNumber} scale={scale} width={breakpoint==='sm'?260:600}/>
        </Document>
        </div>
        <div className="searcher">
      <SearchBar placeholder="Ingrese el ID del modelo a buscar..." data={products} breakpoint={breakpoint}/>
        </div>
      </section>
      {state&& (
        <Joyride 
        steps={state.setps}
        continuous
        showProgress
        showSkipButton
        locale={
          {back: 'Regresar', next: 'Siguiente', skip: 'Saltar guía', last:'Terminar guía'}
        }
        styles={
          {
            options:{
              backgroundColor: '#fff',
              arrowColor: '#fff',
              primaryColor: '#000',
              textColor: '#000',
            }
          }
        }
      />
      )}
      
    </div>
  );
};

export default PDFReader;
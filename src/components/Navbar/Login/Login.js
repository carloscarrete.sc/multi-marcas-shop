import { useEffect } from 'react';
import Logo from '../../../assets/images/login.png'
import {Ingresar} from './Ingresar';

import Aos from 'aos'

const Login = () =>{

    useEffect(() => {
        Aos.init({ duration: 2000 });
    }, [])

    return(
        <div className="container mt-3" data-aos="fade-up"> 
            <div className="row">
                <div className="col-md-7 my-auto">
                    <img src={Logo} className="img-fluid w-100" alt="Registrar usuario"/> 
                </div>
                <div className="col-md-5">
                    <Ingresar />
                </div>
            </div>
        </div>
    )

}

export default Login;
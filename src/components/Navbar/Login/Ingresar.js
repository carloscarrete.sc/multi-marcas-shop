import { Formik, Form } from 'formik'
import React from 'react'
import { TextField } from './Textfield'
import { Button } from 'react-bootstrap';
import * as Yup from 'yup';
import axios from 'axios';
import { useHistory } from 'react-router';
//
import useAuth from '../../../hooks/useAuth';

import {toast} from 'react-toastify'

import Spinner from 'react-bootstrap/Spinner'

import { BASE_PATH } from '../../../utils/constants';

export const Ingresar = () => {

    const history = useHistory();


    const {login} = useAuth();
    const validate = Yup.object({
        email: Yup.string()
            .email('El correo no tiene un formato válido')
            .required('El correo es obligatorio'),
        password: Yup.string()
            .min(6, 'La contraseña debe de ser de al menos 6 dígitos')
            .required('La contraseña es obligatoria'),
    })

    const goToForgotPassoword = () =>{
        history.push('/olvido-contrasena')
    }

    return (
        <Formik
            initialValues={{
                email: '',
                password: ''
            }}
            validationSchema={validate}
            onSubmit={(values, { setSubmitting, resetForm }) => {
                setSubmitting(true)
                axios
                    .post(`${BASE_PATH}/auth/local`,{
                        identifier: values.email,
                        password: values.password
                    })
                    .then(res=>{
                        login(res.data.jwt)
                        toast.success('Bienvenido',{
                            theme: 'colored'
                        })
                        history.push('/')

                    })
                    .catch(error=>{
                        toast.warning('Ha ocurrido un error, por favor verifique su usuario y/o contraseña',{
                            theme: 'colored'
                        })
                        setSubmitting(false)
                    })

            }}
        >
            {formik => (
                <div>
                    <h1 className="my-4 font-weight-bold .display-4">Iniciar Sesión!</h1>
                    <Form>
                        <TextField label="Correo electrónico" name="email" type="email" />
                        <TextField label="Contraseña" name="password" type="password" />
                        <div className="d-flex justify-content-center">
                        <Button className="btn btn-dark m-3" type="submit" disabled={formik.isSubmitting?true:false}>
                        {formik.isSubmitting&&(
                            <Spinner
                            as="span"
                            animation="grow"
                            size="sm"
                            role="status"
                            aria-hidden="true"
                        />
                        )}
                        Iniciar Sesión</Button>
                        <Button className="btn btn-danger m-3" type="reset">Limpiar formulario</Button>
                        <Button className="btn btn-warning m-3" onClick={()=>goToForgotPassoword()}>¿Haz olvidado la contraseña?</Button>
                        </div>
                    </Form>
                </div>
            )}
        </Formik>
    )
}

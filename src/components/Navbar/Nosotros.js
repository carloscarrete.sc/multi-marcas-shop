import React, { useEffect } from 'react'
import Logo from '../../assets/bussiness/MultiMarcas.png'

import Aos from 'aos'


const Nosotros = () => {

    useEffect(() => {
        Aos.init({ duration: 2000 });
    }, [])


    return (
        <div>
            <div className="container">
                <div className="row">
                    <div className="col-md-4 col-xs-4 mt-3">
                    <img src={Logo} alt="Logo Multi-Marcas" className="img-fluid"  data-aos="flip-left"></img>
                    </div>
                    <div className="col-md-8 col-xs-8 mt-3" data-aos="zoom-in">
                        <h2>Multi-Marcas</h2>  
                        <h5 className="text-justify">
                            <strong>Multimarcas, ventas por catálogo</strong> 
                            es una empresa encargada especializada en la venta por catálogos la cual lleva
                            ya más de <strong>6 años</strong> en el mercado trabajando con con una gran diversidad
                            de cátalogos tales como <strong>Andrea, Castalia, Impuls, Incógnita, entre otros...</strong>
                            Con nosotros podrás encontrar una gran cantidad de productos, los cuales puedes encontrar en 
                            nuestra sección de <strong>Catálogos</strong>, en donde podrás todos nuestros catálogos
                            que se encuentran actualmente disponibles.
                            
                        </h5>
                <div className="d-flex justify-content-center mt-5">
          <iframe title="Mapa" src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d29152.896412843344!2d-104.67206376044922!3d24.02711349999999!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x869bb76ec1712885%3A0x556a6560ac04f2ed!2sMultimarcas%20ventas%20por%20catalogo!5e0!3m2!1ses-419!2smx!4v1637799330731!5m2!1ses-419!2smx" width="300" height="225" style={{border:0}} allowFullScreen="" loading="lazy"></iframe>
        </div>
                        </div>
                </div>
            </div>
        </div>
    )
}

export default Nosotros

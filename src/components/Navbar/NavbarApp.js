import React, { useState, useEffect } from 'react'
import Container from 'react-bootstrap/Container'
import { Navbar, Nav } from 'react-bootstrap'
import { Link, useHistory } from 'react-router-dom'
import useAuth from '../../hooks/useAuth'
import { getUserData } from './User'
import useCart from '../../hooks/useCart'


const NavbarApp = () => {

  const [user, setUser] = useState(undefined)

  const { auth, logout } = useAuth()

  const history = useHistory()
  const mainPage = () => {
    logout()
    history.push('/')
  }

  useEffect(() => {
    const userData = async () => {
      const getUserDataFromFetch = await getUserData(logout);
      setUser(getUserDataFromFetch)
    }
    userData()
  }, [auth, logout])

  return (
    <div>
      <Navbar collapseOnSelect expand="lg" bg="dark" variant="dark">
        <Container>
          <Navbar.Brand as={Link} to={'/'}>Multi-Marcas</Navbar.Brand>
          <Navbar.Toggle aria-controls="responsive-navbar-nav" />
          <Navbar.Collapse id="responsive-navbar-nav">
            <Nav className="me-auto">
              <Nav.Link as={Link} to={'/'}>Inicio</Nav.Link>
              <Nav.Link as={Link} to={'nosotros'}>
                Acerca de Nosotros
              </Nav.Link>
              <Nav.Link as={Link} to={'/contacto'}>Contacto</Nav.Link>
              <Nav.Link as={Link} to='/todos'>Catálogos</Nav.Link>
            </Nav>
            {user !== undefined && (
              <ClientMenu user={user} mainPage={mainPage} />
            )}
            <Nav>
            </Nav>
          </Navbar.Collapse>
        </Container>
      </Navbar>
    </div>
  )
}

 function ClientMenu({ user, mainPage }) {
  const { productCart } = useCart()
  return (
    <Nav>
      {user ? (
        <>
          <Nav.Link as={Link} to={'/'} className="ml-auto">{'Bienvenido ' + user.username}</Nav.Link>
          <Nav.Link as={Link} to={'/direccion'} className="ml-auto">Mi dirección</Nav.Link>
          <Nav.Link as={Link} to={'/pedidos'} className="ml-auto">Mis pedidos</Nav.Link>
          <Nav.Link as={Link} to={'/carrito'} className="ml-auto"><i className="fas fa-shopping-cart">{productCart > 0 ? productCart : ""}</i></Nav.Link>
          <Nav.Link onClick={mainPage}>Cerrar Sesión</Nav.Link>
        </>
      ) : (
        <>
          <Nav.Link as={Link} to={'/registrarse'} className="ml-auto">Registrarse</Nav.Link>
          <Nav.Link as={Link} to={'/login'}>Iniciar Sesión</Nav.Link>
        </>
      )}
    </Nav>
  )
}
 
export default NavbarApp
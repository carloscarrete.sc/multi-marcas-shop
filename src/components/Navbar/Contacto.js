import React, { useState } from 'react'
import { Form } from 'react-bootstrap'
import { Formik, Field, ErrorMessage } from 'formik'

const Contacto = () => {
  const [formularioEnviado, formularioCambiadoEnviado] = useState(false)
  return (
    <>
      <div className="contenedor">
        <Formik
          initialValues={{
            nombre: '',
            correo: ''
          }}
          validate={(valores) => {
            let errores = {};
            //Validaciones para el nombre
            if (!valores.nombre) {
              errores.nombre = 'Tienes que agregar tu nombre';
            } else if (!/^[a-zA-ZÀ-ÿ\s]{1,40}$/.test(valores.nombre)) {
              errores.nombre = 'El nombre solo puede contener letras y espacios'
            }
            //Validación Correo
            if (!valores.correo) {
              errores.correo = 'Por favor ingrese un correo electrónico';
            } else if (!/^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+\.[a-zA-Z0-9-.]+$/.test(valores.correo)) {
              errores.correo = 'El correo electrónico solamente puede contener letras, números, puntos, guiones y guión bajo'
            }
            return errores;
          }}
          onSubmit={(valores, { resetForm }) => {
            resetForm()
            formularioCambiadoEnviado(true)
            setTimeout(() => formularioCambiadoEnviado(false), 5000)
          }}
        >
          {({ errors, handleSubmit }) => (
            // {({values, errors, touched, handleSubmit, handleChange, handleBlur})=>(
            <Form className="formulario" onSubmit={handleSubmit}>
              <div>
                <label htmlFor="nombre">Nombre</label>
                <Field type="text" id="nombre" name="nombre" placeholder="Tu nombre" />
                <ErrorMessage name="nombre" component={() => (
                  <div className="error">{errors.nombre}</div>
                )} />
              </div>
              <div>
                <label htmlFor="correo">Correo</label>
                <Field type="email" id="correo" name="correo" placeholder="tucorreo@gmail.com" />
                <ErrorMessage name="correo" component={() => (
                  <div className="error">{errors.correo}</div>
                )} />
              </div>

              <div>
                <label htmlFor="nombre">Seleccione respecto a que tema es su duda</label>
                <Field name="motivo" as="select">
                  <option value="informes">Afiliaciones</option>
                  <option value="informes">Envios</option>
                  <option value="informes">Productos</option>
                </Field>
              </div>

              <div>
                <label htmlFor="nombre">Mensaje</label>
                <Field name="mensaje" as="textarea" placeholder="Escriba en este campo las dudas que tenga" />
              </div>

              <button type="submit">Enviar</button>
              {formularioEnviado && <p className="exito">Formulario enviado con éxito</p>}
            </Form>
          )}
        </Formik>
      </div>
    </>
  )
}

export default Contacto

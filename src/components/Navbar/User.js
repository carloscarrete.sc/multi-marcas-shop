import { toast } from "react-toastify";
import { BASE_PATH } from "../../utils/constants";
import { authFetch } from "../../utils/fetch";
import axios from 'axios';
export async function getUserData(logout) {
    try {
        const url = `${BASE_PATH}/users/me`
        const result = await authFetch(url, null, logout);
        return result ? result : null;
    } catch (error) {
        return null
    }
}

export async function resetPasswordApi(email) {
    axios
        .post(`${BASE_PATH}/auth/forgot-password`, {
            email: email,
            url:
                `${BASE_PATH}/admin/plugins/users-permissions/auth/reset-password`
        })
        .then(response => {
            toast.success('Se ha enviado un enlace para restablecer su contraseña a tu correo electrónico.', {
                theme: "colored"
            })
        })
        .catch(error => {
            toast.error('No se ha podido enviar el enlace para restablecer tu contraseña ya que el correo no se encuentra registrado.', {
                theme: "colored"
            })
        });
}

export async function changePasswordReset(code, password, confirmPassword) {
    axios
        .post(`${BASE_PATH}/auth/reset-password`, {
            code : code,
            password : password,
            passwordConfirmation: confirmPassword
        })
        .then(response => {
            toast.success('Tu contraseña ha sido cambiada con éxito', {
                theme: "colored"
            })
        })
        .catch(error => {
            toast.error('No ha sido posible cambiar tu contraseña',{
                theme: "colored"
            })
        });
}
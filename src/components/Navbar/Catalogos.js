import React from 'react';
import { useEffect, useState } from 'react';
import axios from 'axios';
import Cards from '../Catalogs/Cards';
import { BASE_PATH } from '../../utils/constants';

/* No esta en uso este componente */

const Catalogos = () => {
    const [catalogos, setCatalogos] = useState([])
    
    useEffect(()=>{
        const getCatalogos = async()=>{
            try{
                const res = await axios(`${BASE_PATH}/catalogos-disponibles?_sort=Nombre:ASC`);
                setCatalogos(res.data);

            }catch(error){
                console.log(error)
            }
        }
        getCatalogos();
    },[])

    return (
        <div>
            <Cards catalogos={catalogos} />
        </div>
    )
}

export default Catalogos

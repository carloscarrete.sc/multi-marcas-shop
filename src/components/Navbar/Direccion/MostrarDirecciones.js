import React, { useState, useEffect } from 'react'
import useAuth from '../../../hooks/useAuth'
import {getAdress } from '../../../utils/address'
import Table from 'react-bootstrap/Table'
import {Button } from 'react-bootstrap'
import ModalModify from './ModalModify'
import Modal from './Modal'
import { useHistory } from 'react-router-dom'


const MostrarDirecciones = (props) => {
    /* Se declaran los diferentes estados que estara utilizando este componente
    el cual son las direcciones, el auth que contiene las credenciales del usuario
    y el logout al momento de cerrar la sesión. Además de un State para actualizar
    cada vez que se agregue, se elimine o se modifique una dirección */


    

    const [addresses, setAddresses] = useState(null)
    const { auth, logout } = useAuth()
/*     const [valueFromSelect, setSetValueFromSelect] = useState(null)
 */    const [addressId, setAddressId] = useState(null)


    const { reloadAddress, setReloadAddress } = props;

    const [modalOpen, setModalOpen] = useState(false);
    const [modalOpenEdit, setModalOpenEdit] = useState(false)

    const [userActive, setUserActive] = useState(undefined)
    const history = useHistory();

    useEffect(() => {
        (async () => {
            if(auth!==null){
                const response = await getAdress(auth.idUser, logout)
                setAddresses(response || [])
                setReloadAddress(false)
            }else{
                setUserActive(true)
            }
        })()
    }, [reloadAddress, logout, auth, setReloadAddress])

    if (userActive) return null;
    if (!userActive && !auth){
        history.push('/');
        return null;
    }
    if (!addresses) return null

    const setIdForModalDeleteAddress = async (idUser) =>{
        setAddressId(idUser)
        setModalOpen(true)
    }

    const setIfForModalModifyAddress = async (idAddress) =>{
        setAddressId(idAddress)
        setModalOpenEdit(true)
    }

    return (
        <div>
                  {modalOpen && <Modal setOpenModal={setModalOpen} addressId={addressId} logout={logout} setReloadAddress={setReloadAddress}/>}
                  {modalOpenEdit && <ModalModify setModalOpenEdit={setModalOpenEdit} addressId={addressId} logout={logout} setReloadAddress={setReloadAddress}/>}

            {props.modify ? (
                /* El siguiente código es para realizar una validación en dado caso de que a la
                prop de modify en ParentAdress Exista, realizara lo siguiente:
                Estara mostrando en un select las direcciones que tiene el cliente actualmente */
                <div>
            {/*     <FloatingLabel controlId="floatingSelect" label="Seleccione una dirección para modificar">
                    <Form.Select aria-label="Floating label select example" onChange={handleOnChange.bind(this)}>
                            <option key="default" value="default">Selecciona una dirección</option>
                        {addresses.map(item=>{
                            return(
                                <option key={item.id} value={item.value}>{item.titulo}</option>
                            )
                        })}
                    </Form.Select>
                </FloatingLabel> */}
{/*                  {<>
                 {addresses.map(item=>{
                     addresses.find(item=>item.titulo===valueFromSelect&& <h1>{item.titulo}</h1>)
                 })}
                 </>} */}
            </div>
        

            ) : (
                <>
                    {addresses.length !== 0 ?
                        (
                            <Table striped bordered hover size="sm" responsive>
                                <thead>
                                    <tr>
                                        <th>Título</th>
                                        <th>Nombre</th>
                                        <th>Dirección</th>
                                        <th>Ciudad</th>
                                        <th>Estado</th>
                                        <th>Código Postal</th>
                                        <th>Teléfono</th>
                                        <th>Eliminar</th> 
                                        <th>Modificar</th> 
                                    </tr>
                                </thead>
                                <tbody>
                                    {addresses.map(item => {
                                        return (
                                            <tr key={item.id}>
                                                <td>{item.titulo}</td>
                                                <td>{item.nombre}</td>
                                                <td>{item.direccion}</td>
                                                <td>{item.ciudad}</td>
                                                <td>{item.estado}</td>
                                                <td>{item.codigoPostal}</td>
                                                <td>{item.telefono}</td>
                                                <td><Button variant="danger" onClick={()=>setIdForModalDeleteAddress(item.id)}><i className="fas fa-trash-alt"></i></Button></td>
                                                <td><Button variant="danger" onClick={()=>setIfForModalModifyAddress(item.id)}><i className="fas fa-edit"></i></Button></td>
                                            </tr>
                                        )
                                    })}
                                </tbody>
                            </Table>
                        ) : <div>No tienes ninguna dirección agregada. Puedes agregar una dirección haciendo click en el botón de "Agregar Nueva Dirección".</div>}
                </>
            )}
        </div>
    )

    
  /*   function handleOnChange(event){
            setSetValueFromSelect(event.target.value)
    } */
}

export default MostrarDirecciones

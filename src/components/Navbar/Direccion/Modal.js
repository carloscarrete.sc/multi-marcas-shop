
import React from "react";
import "./ModalDelete.css";


import { deleteAddressFromUser } from '../../../utils/address'


function Modal({ setOpenModal, addressId, logout, setReloadAddress }) {
  return (
    <div className="modalBackground">
      <div className="modalContainer">
        <div className="titleCloseBtn">
          <button
            onClick={() => {
              setOpenModal(false);
            }}
          >
            X
          </button>
        </div>
        <div className="title">
          <h3>¿Esta seguro que desea eliminar la dirección seleccionada?</h3>
        </div>
        <div className="body">
          <p>Se eliminará la dirección de su lista si usted desea continuar</p>
        </div>
        <div className="footer">
          <button
            onClick={() => {
              setOpenModal(false);
            }}
            id="cancelBtn"
          >
            Cancelar
          </button>
          <button
            onClick={async () => {
              const response = await deleteAddressFromUser(addressId, logout);
              if (response) setReloadAddress(true)
              setOpenModal(false)
            }}
          >Continuar</button>
        </div>
      </div>
    </div>
  );
}

export default Modal;
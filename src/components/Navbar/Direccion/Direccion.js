import { useEffect } from 'react'
import Logo from '../../../assets/images/address.png'
import ParentAddress from './ParentAdress'

import Aos from 'aos'


const Direccion = () => {
    useEffect(() => {
        Aos.init({ duration: 2000 });
    }, [])

    return (
        <div className="container mt-3" data-aos="fade-right">
            <div className="row">
               <div className="col-md-4 my-auto" >
                   <img src={Logo} alt="Dirección del cliente" className="img-fluid w-100" />
               </div>
{/*                my-auto fue removido
 */}               <div className="col-md-8 mt-3" data-aos="fade-up"> 
                <ParentAddress />
               </div>
            </div>
        </div>
    )
}

export default Direccion

import React, {useState, useEffect} from 'react';
import * as Yup from 'yup';
import { Formik, Form } from 'formik'
import { TextField } from './Textfield';
import useAuth from '../../../hooks/useAuth';
import {  getSingleAdress, updateAddress } from '../../../utils/address';


const ModalModify = ({ setModalOpenEdit, addressId, logout, setReloadAddress }) => {

    const [modificado, setModificado] = useState(false)
    const { auth } = useAuth()    
    const [singleAddress, setsingleAddress] = useState(null)


    const phoneRegExp = /^((\\+[1-9]{1,4}[ \\-]*)|(\\([0-9]{2,3}\\)[ \\-]*)|([0-9]{2,4})[ \\-]*)*?[0-9]{3,4}?[ \\-]*[0-9]{3,4}?$/


    const actualizarDireccion = (data) => {
        const dataTemp = {
            ...data,
            user: auth.idUser,
        };
        const response =  updateAddress(singleAddress.id,dataTemp, logout)
        if (!response) {
            console.log('Error al actualizar la dirección')
        } else {
            setReloadAddress(true)
        }
    }

    const validate = Yup.object({
        titulo: Yup.string()
            .max(20, 'El título de su dirección debe de ser de menos de 20 carácteres')
            .required('El título es obligatorio')
            .matches(/^[aA-zZ\s]+$/, "Solo puedes escribir texto en este campo"),
        nombre: Yup.string()
            .max(30, 'Su nombre debe de tener menos de 30 carácteres')
            .required('El nombre es obligatorio')
            .matches(/^[aA-zZ\s]+$/, "Solo puedes escribir texto en este campo"),
        direccion: Yup.string()
            .max(50, 'La dirección de su casa debe de tener menos de 50 carácteres')
            .required('La dirección de su casa es obligatoria'),
        ciudad: Yup.string()
            .max(20, 'La ciudad debe de tener menos de 20 carácteres')
            .required('La ciudad es obligatorio')
            .matches(/^[aA-zZ\s]+$/, "Solo puedes escribir texto en este campo"),
        estado: Yup.string()
            .max(25, 'El Estado debe de tener menos de 25 carácteres')
            .required('El estado es obligatorio')
            .matches(/^[aA-zZ\s]+$/, "Solo puedes escribir texto en este campo"),
        codigoPostal: Yup.string()
            .max(10, 'El Código Postal debe de tener menos de 10 carácteres')
            .required('El Código Postal es obligatorio')
            .matches(/^\d+(\.\d{1,2})?$/, 'Este camó solamente acepta valores numéricos'),
        telefono: Yup.string()
            .max(15, 'El número de teléfono debe de tener menos de 15 carácteres')
            .required('El teléfono es obligatorio')
            .matches(phoneRegExp, 'El número de teléfono no tiene un formato válido')
    })

    useEffect(() => {
        (async ()=>{
            const response = await getSingleAdress(addressId, logout);
            setsingleAddress(response)
        })()
    }, [addressId, logout])

    if(!singleAddress) return null;


    return (
        <div>
            <div className="modalBackground">
                <div className="modalContainer">
                    <div className="titleCloseBtn">
                        <button
                            onClick={() => {
                                setModalOpenEdit(false);
                            }}
                        >
                            X
                        </button>
                    </div>
                    <div className="title">
                        <h3>Modificar la dirección seleccionada</h3>
                    </div>
                    <div className="body">
                    <Formik
                    initialValues={{
                        titulo: singleAddress.titulo,
                        nombre: singleAddress.nombre,
                        direccion: singleAddress.direccion,
                        ciudad: singleAddress.ciudad,
                        estado: singleAddress.estado,
                        codigoPostal: singleAddress.codigoPostal,
                        telefono: singleAddress.telefono
                    }}
                    validationSchema={validate}
                    onSubmit={(values, { resetForm }) => {
                        actualizarDireccion(values)
                        setTimeout(() => setModificado(false), 10000)
                        //resetForm()
                    }}
                >
                    {formik => (
                        <div>
                            <Form>
                                {
                                }
                                <TextField label="Título de mi dirección" name="titulo" type="text" ph={singleAddress.titulo} />
                                <div className="row">
                                    <TextField label="Nombre(s) y Apellidos" name="nombre" type="text" sm={1} ph={singleAddress.nombre} />
                                    <TextField label="Dirección" name="direccion" type="text" sm={1} ph={singleAddress.direccion} />
                                    <TextField label="Ciudad" name="ciudad" type="text" sm={1} ph={singleAddress.ciudad}/>
                                    <TextField label="Estado/Región/Provincia" name="estado" type="text" sm={1} ph={singleAddress.estado}/>
                                    <TextField label="Código Postal" name="codigoPostal" type="text" sm={1} ph={singleAddress.codigoPostal}/>
                                    <TextField label="Teléfono" name="telefono" type="text" sm={1} ph={singleAddress.telefono}/>
                                </div>
                                <div className="d-flex justify-content-center">
                                    <button className="btn btn-dark m-3" type="submit">Editar dirección</button>
                                    <button className="btn btn-danger m-3" type="reset" onClick={()=>setModalOpenEdit(false)}>Cancelar</button>
                                </div>
                                {modificado && <p className="exito">Se ha modificado la dirección de manera exitosa</p>}
                            </Form>
                        </div>
                    )}
                </Formik>
                    </div>
                </div>
            </div>
        </div>
    )
}

export default ModalModify

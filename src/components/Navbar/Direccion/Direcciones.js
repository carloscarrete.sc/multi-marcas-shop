import React, { useState, useRef } from 'react'
import './Collapsible.css'



const Direcciones = (props) => {
    const [isOpen, setIsOpen] = useState(false)
    const parentRef = useRef()

    return (
        <div className="collapsible">
            <div className="d-flex justify-content-center">
            {props.title===undefined?(<button className="toggle" onClick={() => setIsOpen(!isOpen)}>{props.label}</button>):
            <div>
                <h1>
                {props.title}
                </h1>
                <h5>
                {props.description}
                </h5>
            </div>}
            </div>
            <div className="content-parent" ref={parentRef} style={isOpen ? {
                height: parentRef.current.scrollHeight+0+"px"
            } : {
                height: "0px"
            }}>
                <div className="content">
                    {props.children}
                </div>
            </div>
        </div>
    )
}



export default Direcciones

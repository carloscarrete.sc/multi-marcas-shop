import React, { useState } from 'react'
import Direcciones from './Direcciones';
import { Formik, Form } from 'formik'
import { TextField } from './Textfield'
import * as Yup from 'yup';
import useAuth from '../../../hooks/useAuth';
import { crearDireccionNueva } from '../../../utils/address';
import MostrarDirecciones from './MostrarDirecciones';

function ParentAddress() {
    const { auth, logout } = useAuth()
    const [registrado, setRegistrado] = useState(false)
    const [reloadAddress, setReloadAddress] = useState(false)

/*     const [loading, setLoading] = useState(false)
 */ const phoneRegExp = /^((\\+[1-9]{1,4}[ \\-]*)|(\\([0-9]{2,3}\\)[ \\-]*)|([0-9]{2,4})[ \\-]*)*?[0-9]{3,4}?[ \\-]*[0-9]{3,4}?$/


    const validate = Yup.object({
        titulo: Yup.string()
            .max(20, 'El título de su dirección debe de ser de menos de 20 carácteres')
            .required('El título es obligatorio')
            .matches(/^[aA-zZ\s]+$/, "Solo puedes escribir texto en este campo"),
        nombre: Yup.string()
            .max(30, 'Su nombre debe de tener menos de 30 carácteres')
            .required('El nombre es obligatorio')
            .matches(/^[aA-zZ\s]+$/, "Solo puedes escribir texto en este campo"),
        direccion: Yup.string()
            .max(50, 'La dirección de su casa debe de tener menos de 50 carácteres')
            .required('La dirección de su casa es obligatoria'),
        ciudad: Yup.string()
            .max(20, 'La ciudad debe de tener menos de 20 carácteres')
            .required('La ciudad es obligatorio')
            .matches(/^[aA-zZ\s]+$/, "Solo puedes escribir texto en este campo"),
        estado: Yup.string()
            .max(25, 'El Estado debe de tener menos de 25 carácteres')
            .required('El estado es obligatorio')
            .matches(/^[aA-zZ\s]+$/, "Solo puedes escribir texto en este campo"),
        codigoPostal: Yup.string()
            .max(10, 'El Código Postal debe de tener menos de 10 carácteres')
            .required('El Código Postal es obligatorio')
            .matches(/^\d+(\.\d{1,2})?$/, 'Este camó solamente acepta valores numéricos'),
        telefono: Yup.string()
            .max(15, 'El número de teléfono debe de tener menos de 15 carácteres')
            .required('El teléfono es obligatorio')
            .matches(phoneRegExp, 'El número de teléfono no tiene un formato válido')
    })

    const crearDireccion = async (data) => {
/*         setLoading(true)
 */        const dataTemp = {
            ...data,
            user: auth.idUser,
        };
        const response = await crearDireccionNueva(dataTemp, logout)
        if (!response) {
            console.log('Mensaje de error')
        } else {
            setReloadAddress(true)
        }
    }
    return (
        <div>
            <Direcciones title="Direcciones" description="En esta sección podrás observar
            cuales son las direcciones que tienes agregradas, agregar nuevas direcciones o modificar
             las existentes"/>
            <Direcciones label="Mis direcciones / Modificar / Eliminar">
                <MostrarDirecciones  reloadAddress={reloadAddress} setReloadAddress={setReloadAddress}/>
            </Direcciones>
            <Direcciones label="Agrear Nueva Dirección">
                <Formik
                    initialValues={{
                        titulo: '',
                        nombre: '',
                        direccion: '',
                        ciudad: '',
                        estado: '',
                        codigoPostal: '',
                        telefono: ''
                    }}
                    validationSchema={validate}
                    onSubmit={(values, { resetForm }) => {
                        crearDireccion(values)
                        setTimeout(() => setRegistrado(false), 10000)
                        resetForm()
                    }}
                >
                    {formik => (
                        <div>
                            <div className="d-flex justify-content-center">
                            <h3 className="my-4 font-weight-bold .display-4">Agregar dirección</h3>
                            </div>
                            <Form>
                                <TextField label="Título de mi dirección" name="titulo" type="text" />
                                <div className="row">
                                    <TextField label="Nombre(s) y Apellidos" name="nombre" type="text" sm={1} />
                                    <TextField label="Dirección" name="direccion" type="text" sm={1} />
                                    <TextField label="Ciudad" name="ciudad" type="text" sm={1} />
                                    <TextField label="Estado/Región/Provincia" name="estado" type="text" sm={1} />
                                    <TextField label="Código Postal" name="codigoPostal" type="text" sm={1} />
                                    <TextField label="Teléfono" name="telefono" type="text" sm={1} />
                                </div>
                                <div className="d-flex justify-content-center">
                                    <button className="btn btn-dark m-3" type="submit">Registrar mi dirección</button>
                                    <button className="btn btn-danger m-3" type="reset">Limpiar formulario</button>
                                </div>
                                {registrado && <p className="exito">Se ha registrado de manera exitosa</p>}
                            </Form>
                        </div>
                    )}
                </Formik>
            </Direcciones>
 {/*            <Direcciones label="Modificar dirección">
               <MostrarDirecciones reloadAddress={reloadAddress} setReloadAddress={setReloadAddress} modify="si"/> 
            </Direcciones>
            <Direcciones label="Eliminar dirección">
               <MostrarDirecciones reloadAddress={reloadAddress} setReloadAddress={setReloadAddress} modify="si"/> 
            </Direcciones> */}
        </div>
    )
}

export default ParentAddress;
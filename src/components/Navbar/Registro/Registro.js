import Logo from '../../../assets/images/register.png'
import {Registrarse} from './Registrarse';
import { useEffect } from 'react';

import Aos from 'aos'
import { Link } from 'react-router-dom';


const Registro = () =>{

    useEffect(() => {
        Aos.init({ duration: 2000 });
    }, [])


    return(
        <div className="container mt-3" data-aos="fade-up"> 
            <div className="row">
                <div className="col-md-5 order-1 order-md-0">
                    <Registrarse />
                    <div className="mb-5">
                        <h6>Al registrarte aceptas <Link to="/terminos-y-condiciones">los términos y condiciones </Link> 
                        y las <Link to="/politicas-de-privacidad">políticas de privacidad</Link> de la tienda en línea multi-marcas</h6>
                    </div>
                </div>
                <div className="col-md-7 my-auto order-0 order-md-1">
                    <img src={Logo} className="img-fluid w-100" alt="Registrar usuario"/> 
                </div>
            </div>
        </div>
    )

}

export default Registro;
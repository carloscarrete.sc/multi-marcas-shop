import { Formik, Form } from 'formik'
import React from 'react'
import { TextField } from './Textfield'
import * as Yup from 'yup';
import axios from 'axios';

import {toast} from 'react-toastify'

import Spinner from 'react-bootstrap/Spinner'
import { Button } from 'react-bootstrap';

import { BASE_PATH } from '../../../utils/constants';

export const Registrarse = () => {

/*     const [registrado, setRegistrado] = useState(false)
 */
    const validate = Yup.object({
        primerNombre: Yup.string()
            .max(15, 'El nombre debe debe ser de menos de 15 caracteres')
            .required('El nombre es obligatorio'),
        segundNombre: Yup.string()
            .max(20, 'El segundo nombre debe de ser de menos de 15 caracteres'),
        apellidoPaterno: Yup.string()
            .max(15, 'El apellido paterno debe debe ser de menos de 15 caracteres')
            .required('El apellido paterno es obligatorio'),
        apellidoMaterno: Yup.string()
            .max(20, 'El apellido materno debe de ser de menos de 15 caracteres')
            .required('El apellido materno es obligatorio'),
        email: Yup.string()
            .email('El correo no tiene un formato válido')
            .required('El correo es obligatorio'),
        password: Yup.string()
            .min(6, 'La contraseña debe de ser de al menos 6 dígitos')
            .required('La contraseña es obligatoria'),
        username: Yup.string()
            .max(15, 'El nombre de usuario debe de ser de menos de 15 caracteres')
            .required('El nombre de usuario es obligatorio'),
        confirmPassword: Yup.string()
            .oneOf([Yup.ref('password'), null], 'No coinciden las contraseñas')
            .required('Debe de confirmar su contraseña'),
    })

    return (
        <Formik
            initialValues={{
                primerNombre: '',
                segundNombre: '',
                apellidoPaterno: '',
                apellidoMaterno: '',
                email: '',
                password: '',
                confirmPassword: '',
                username : ''
            }}
            validationSchema={validate}
            onSubmit={(values, { setSubmitting ,resetForm }) => {
                setSubmitting(true)
                axios
                    .post(`${BASE_PATH}/auth/local/register`, {
                        Nombre1: values.primerNombre,
                        Nombre2: values.segundNombre,
                        ApellidoP: values.apellidoPaterno,
                        ApellidoM: values.apellidoMaterno,
                        username: values.username,
                        email: values.email,
                        password: values.password,
                    })
                    .then(response => {
                        toast.success('Se ha registrado de manera exitosa, ahora puede iniciar sesión',{
                            theme: 'colored',     
                        })
                        resetForm();
                    })
                    .catch(error => {
                        toast.warning('Ha ocurrido un error, no se ha podido realizar el registro',{
                            theme: 'colored'
                        })
                        setSubmitting(false)
                    });
                
                /* setRegistrado(true);
                setTimeout(()=>setRegistrado(false),10000) */
            }}
        >
            {formik => (
                <div>
                    <h1 className="my-4 font-weight-bold .display-4">¡Registrate!</h1>
                    <Form>
                        <div className="row">
                            <TextField label="Primer Nombre" name="primerNombre" type="text" sm={1} />
                            <TextField label="Segundo Nombre" name="segundNombre" type="text" sm={1} />
                            <TextField label="Apellido Paterno" name="apellidoPaterno" type="text" sm={1} />
                            <TextField label="Apellido Materno" name="apellidoMaterno" type="text" sm={1} />
                        </div>
                        <TextField label="Nombre de usuario" name="username" type="text" />
                        <TextField label="Correo electrónico" name="email" type="email" />
                        <TextField label="Contraseña" name="password" type="password" />
                        <TextField label="Confirmar contraseña" name="confirmPassword" type="password" />
                        <div className="d-flex justify-content-center">
                        <Button className="btn btn-dark m-3" type="submit" disabled={formik.isSubmitting?true:false}>
                        {formik.isSubmitting&&(
                            <Spinner
                            as="span"
                            animation="grow"
                            size="sm"
                            role="status"
                            aria-hidden="true"
                        />
                        )}
                        Registrarse</Button>
                        <Button className="btn btn-danger m-3" type="reset">Limpiar formulario</Button>
                        </div>
{/*                         {registrado && <p className="exito">Se ha registrado de manera exitosa</p>}
 */}                    </Form>
                </div>
            )}
        </Formik>
    )
}

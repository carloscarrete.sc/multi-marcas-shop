import React, {useState, useEffect} from 'react'
import useAuth from '../../hooks/useAuth'
import {getOrdersByApi} from '../../utils/orders'

import Table from 'react-bootstrap/Table'

import DefaultImage from '../../assets/images/bag.png'
import './Pedidos.css'

import Orders from '../../assets/images/orders.png'
import { Link, useHistory } from 'react-router-dom'

import Aos from 'aos'



const Pedidos = () => {

    useEffect(() => {
        Aos.init({ duration: 2000 });
    }, [])


    const [order, setOrder] = useState(null)
    const {auth, logout} = useAuth()

    const [userActive, setUserActive] = useState(undefined)
    const history = useHistory();

    useEffect(() => {
        (async()=>{
            if(auth!==null){
                const response = await getOrdersByApi(auth.idUser, logout)
                setOrder(response || [])
            }else{
                setUserActive(true)
            }
        })()
    }, [logout, auth])

    if (userActive) return null;
    if (!userActive && !auth){
        history.push('/');
        return null;
    }
    if(!order) return null;

    return (
        <div data-aos="fade-up">
            <div className="container mt-3">
                <div className="row">
                    <div className="col-md-8 order-0 order-md-0">
                    <div className="d-flex justify-content-center mt-3">
                    {order.length===0?(
                        <h3>Aún no haz realizado ninguna compra.</h3>
                    ):
                    (<OrderList order={order}/>)}
                </div>
                    </div>

                    <div className="col-md-4 mt-5 order-1 order-md-1">
                        <img src={Orders} alt="Ordenes" className="img-fluid w-100"></img>
                    </div>
                </div>
            </div>
            <div className="container">
                
            </div>
        </div>
    )
}

function OrderList({order}){
    return(
        <div>
                <h1 className="mt-3">Historial de compras realizadas</h1>
                <Table variant="light" striped bordered hover size="sm" responsive>
                    <thead>
                        <tr>
                            <th></th>
                            <th>Producto</th>
                            <th>Precio</th>
                            <th>Direccion</th>
                            <th>Recibe</th>
                            <th>Estado del envio</th>
                        </tr>
                    </thead>
                    <tbody>
                        {order.map(item=>{
                            /* return(
                                <tr key={item[0].id}>
                                    <td>{item[0].totalPago}</td>
                                    <td>{'$ '+ item[0].Precio +' MXN'}</td>
                                </tr>
                            ) */
                            return(
                            <tr key={item.id}>
                                <td><img alt="Logo" src={DefaultImage} className="pedidos"></img></td>
                                <td>{item.producto.Nombre?item.producto.Nombre:<h5>Pedido no encontrado, contacte a soporte</h5>}</td>
                                <td>{item.producto.Precio?item.producto.Precio.toFixed(2):<h5>Pedido no encontrado, contacte a soporte</h5>}</td>
                                <td>{item.DireccionEnvio.direccion+' ' +item.DireccionEnvio.ciudad + ', ' +item.DireccionEnvio.estado+ ', '+item.DireccionEnvio.codigoPostal}</td>
                                <td>{item.DireccionEnvio.nombre}</td>
                                <td>{item.Estado}</td>
                            </tr>
                            )
                        })}
{/*                                 <tr key="total">
                                    <td>Total: </td>
                                    <td></td>
                                </tr> */}
                    </tbody>
                </Table>

                <div className="mt-5">
                    Su pedido estará llegando <strong>1 semana despúes aproxiḿadamente</strong> luego de que haya realizado su compra.
                     Si tienes alguna duda por favor manda tu duda haciendo click <Link to='/contacto'>aquí</Link> o dirigiendote a la página de <strong>contacto</strong>.
                </div>
            </div>
    )
}

export default Pedidos

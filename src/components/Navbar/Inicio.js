import React, { useEffect } from 'react'
import Aos from 'aos'
import Logo from '../../assets/bussiness/MultiMarcas.png'

import Andrea from '../../assets/bussiness/Logos/Andrea.jpg'
import Castalia from '../../assets/bussiness/Logos/Castalia.jpg'
import Cklass from '../../assets/bussiness/Logos/CKLASS.jpg'
import Impuls from '../../assets/bussiness/Logos/Impuls.jpg'
import Incognita from '../../assets/bussiness/Logos/Incognita.png'
import Lis from '../../assets/bussiness/Logos/Lis.png'
import ModaClub from '../../assets/bussiness/Logos/ModaC.png'
import PriceShoes from '../../assets/bussiness/Logos/PriceS.jpg'
import Rinna from '../../assets/bussiness/Logos/Rinna.png'
import Terra from '../../assets/bussiness/Logos/Terra.png'

import './styleHome.css'

import Card from 'react-bootstrap/Card'


/* import PDFReader from '../PDF/PDFReader'
 */const Inicio = () => {

    useEffect(() => {
        Aos.init({ duration: 2000 });
    }, [])


    return (
        <div className="container">
            
            {/*             <PDFReader />
 */}        <div className="row">
                <div className="col-md-8 mt-5">
                    <h1 >Bienvenido</h1>
                    <h3 data-aos="fade-up" >Multi-Marcas es una empresa la cual esta enfocada en la venta de productos por catálogo</h3>
                </div>
                <div className="col-md-4 mt-5">
                    <img data-aos="fade-right" src={Logo} alt="Multi-marcas" className="img-fluid w-100"></img>
                </div>
                <h4 data-aos="zoom-in" data-aos-offset="190">En <strong>Multimarcas, ventas por catálogo</strong> podrás encontrar una gran variedad de
                    cátalogos donde podrás visualizar en formato PDF el catálogo de tu elección, por lo cual de esta
                    manera podrá visualizar los productos de manera digital.
                    Actualmente contamos con <strong>10 marcas de cátalogos</strong> dónde seguramente encontrará el
                    producto ideal que esta buscando.</h4>
            </div>
            {/* ANDREA, texto Izquierda */}
            <div className="col-md-12 mt-5" data-aos="fade-up" data-aos-offset="190">
                    <Card className="w-100">
                        <Card.Header>Andrea</Card.Header>
                        <Card.Body> 
                            <div className="row">
                            <div className="col-md-10 order-1 order-md-0">
                            <Card.Text className="myTech">
                            
                                            <strong>Andrea</strong> es una empresa 100% mexicana, líder en venta por catálogo y con 45 años de
                                            experiencia en el mercado; con presencia en México y en Estados Unidos, preocupada por atraer a
                                            los mejores talentos para la creación y desarrollo de productos de calidad.
                                        
                            </Card.Text>
                                </div> 
                            <div className="col-md-2 order-0 order-md-1">
                            <Card.Text>
                            <img src={Andrea} alt="Andrea Catálogo" className="img-fluid w-100 img-thumbnail"></img>
                            </Card.Text>
                                </div> 
                            </div>  
                        </Card.Body>
                    </Card>
                </div>

                {/* CASTALIA, texto Izquierda */}
            <div className="col-md-12 mt-5" data-aos="fade-up" data-aos-offset="190">
                    <Card className="w-100">
                        <Card.Header>Castalia</Card.Header>
                        <Card.Body> 
                            <div className="row">
                            <div className="col-md-10 order-1 order-md-1">
                            <Card.Text className="myTech">
                            <strong>Castalia</strong> es una empresa 100 % mexicana con más de 40 años de experiencia en la industria del calzado y moda.
                        Se dedican principalmente a la venta de calzado y ropa por catálogo. <strong>Castalia</strong> es una empresa la cual se esfuerza
                        día con día para ofrecer los mejores productos con las últimas tendencias de moda a precios muy atractivos a sus clientes.             
                            </Card.Text>
                                </div> 
                            <div className="col-md-2 order-0 order-md-0">
                            <Card.Text>
                            <img src={Castalia} alt="Castalia Catálogo" className="img-fluid w-100 img-thumbnail"></img>
                            </Card.Text>
                                </div> 
                            </div>  
                        </Card.Body>
                    </Card>
                </div>
                {/* CKLASS, texto Izquierda */}
            <div className="col-md-12 mt-5" data-aos="fade-up" data-aos-offset="190">
                    <Card className="w-100">
                        <Card.Header>Cklass</Card.Header>
                        <Card.Body> 
                            <div className="row">
                            <div className="col-md-10 order-1 order-md-0">
                            <Card.Text className="myTech">
                            
                            <strong>CKLASS</strong> es una empresa 100% mexicana con más de 25 años en la venta por catálogo. Su
                        principal visión es seguir apoyando a la economía mexicana ofreciendo servicio de calidad, innovar con productos de alta moda y
                        crecer día a día junto con nuestros afiliados para ser líderes en el mercado.
                                        
                            </Card.Text>
                                </div> 
                            <div className="col-md-2 order-0 order-md-1">
                            <Card.Text>
                            <img src={Cklass} alt="Cklass Catálogo" className="img-fluid w-100 img-thumbnail"></img>
                            </Card.Text>
                                </div> 
                            </div>  
                        </Card.Body>
                    </Card>
                </div>
                {/* IMPULS, texto Izquierda */}
            <div className="col-md-12 mt-5" data-aos="fade-up" data-aos-offset="190">
                    <Card className="w-100">
                        <Card.Header>Impuls</Card.Header>
                        <Card.Body> 
                            <div className="row">
                            <div className="col-md-10 order-1 order-md-1">
                            <Card.Text className="myTech">
                            <strong>Impuls</strong> es una empresa que cuenta con más de 40 años de experiencia
                        en la industria del calzado en México, la cual les permite mantener una alianza con distintos
                        proveedores para ofrecer a el cliente calidad y moda a precios muy competitivos.
                        Su principal misión es comercializar calzado con el mejor servicio, moda y calidad
                        para generar valor a clientes, colaboradores, proveedores y accionistas.
                            </Card.Text>
                                </div> 
                            <div className="col-md-2 order-0 order-md-0">
                            <Card.Text>
                            <img src={Impuls} alt="Impuls Catálogo" className="img-fluid w-100 img-thumbnail"></img>
                            </Card.Text>
                                </div> 
                            </div>  
                        </Card.Body>
                    </Card>
                </div>
                {/* INCOGNITA, texto Izquierda */}
            <div className="col-md-12 mt-5" data-aos="fade-up" data-aos-offset="190">
                    <Card className="w-100">
                        <Card.Header>Incógnita</Card.Header>
                        <Card.Body> 
                            <div className="row">
                            <div className="col-md-10 order-1 order-md-0">
                            <Card.Text className="myTech">
                            
                            <strong>Incógnita</strong> es una marca innovadora en ventas por catálogo dirigida al mercado joven y
                        especializada en moda y vanguardia. <strong>Incognita</strong> es una empresa con más de 20 años de experiencia
                        en el mercado de venta por catálogo de zapatos, ropa y blancos lo que les ha permitido lograr los mejores precios
                        y calidad en productos de moda, lo que se traduce en una excelente oportunidad de negocio para
                        afiliados y mejor relación precio-valor a nuestros sus clientes siempre enfocados en brindar un servicio personalizado y de calidad.
                                        
                            </Card.Text>
                                </div> 
                            <div className="col-md-2 order-0 order-md-1">
                            <Card.Text>
                            <img src={Incognita} alt="Incógnita Catálogo" className="img-fluid w-100 img-thumbnail"></img>
                            </Card.Text>
                                </div> 
                            </div>  
                        </Card.Body>
                    </Card>
                </div>
                {/* LIS, texto Izquierda */}
            <div className="col-md-12 mt-5" data-aos="fade-up" data-aos-offset="190">
                    <Card className="w-100">
                        <Card.Header>Lis</Card.Header>
                        <Card.Body> 
                            <div className="row">
                            <div className="col-md-10 order-1 order-md-1">
                            <Card.Text className="myTech">
                            <strong>Lis</strong> es una empresa 100% mexicana que ofrece una excelente oportunidad de crecimiento para todas las mujeres dispuestas a superarse.
                        La empresa <strong>Lis</strong> busca apoyarlas con un atractivo sistema de ventas de calzado por catálogo que las impulse a lograr su libertad económica.
                        <strong>Lis</strong> se esfuerza día a día para ser líderes en venta de calzado por catálogo, entendiendo las necesidades de sus clientas.
                            </Card.Text>
                                </div> 
                            <div className="col-md-2 order-0 order-md-0">
                            <Card.Text>
                            <img src={Lis} alt="Lis Catálogo" className="img-fluid w-100 img-thumbnail"></img>
                            </Card.Text>
                                </div> 
                            </div>  
                        </Card.Body>
                    </Card>
                </div>
                {/* MODA CLUB, texto Izquierda */}
            <div className="col-md-12 mt-5" data-aos="fade-up" data-aos-offset="190">
                    <Card className="w-100">
                        <Card.Header>Moda Club</Card.Header>
                        <Card.Body> 
                            <div className="row">
                            <div className="col-md-10 order-1 order-md-0">
                            <Card.Text className="myTech">
                            
                            <strong>Moda Club</strong> es una empresa 100% mexicana con más de 10 años de experiencia en la venta directa de ropa para dama por catálogo y manejan un
                        Plan de Negocio Multinivel. Su principal objetivo es ofrecer a nuestros clientes la oportunidad de disfrutar de sus productos e incluso hacer negocio con
                        ellos. La empresa está principalmente enfocada a la ropa femenina.
                                        
                            </Card.Text>
                                </div> 
                            <div className="col-md-2 order-0 order-md-1">
                            <Card.Text>
                            <img src={ModaClub} alt="Moda Club Catálogo" className="img-fluid w-100 img-thumbnail"></img>
                            </Card.Text>
                                </div> 
                            </div>  
                        </Card.Body>
                    </Card>
                </div>
                {/* PRICE SHOES, texto Izquierda */}
                <div className="col-md-12 mt-5" data-aos="fade-up" data-aos-offset="190">
                    <Card className="w-100">
                        <Card.Header>Price Shoes</Card.Header>
                        <Card.Body> 
                            <div className="row">
                            <div className="col-md-10 order-1 order-md-1">
                            <Card.Text className="myTech">
                            <strong>Price Shoes</strong> es la colección de Calzado y Ropa más grande del mundo, con más de 170,000 m2 de piso comercial.
                        Tienen las mejores marcas Nacionales e Internacionales. <strong>Price Shoes</strong> ofrece más de 35,000 productos, dividido en
                        100 diferentes versiones de catálogos. Además de que tiene 17 tiendas grandes divididas estratégicamente en el interior de la
                        república y 22 tiendas comunidad.
                            </Card.Text>
                                </div> 
                            <div className="col-md-2 order-0 order-md-0">
                            <Card.Text>
                            <img src={PriceShoes} alt="Price Shoes Catálogo" className="img-fluid w-100 img-thumbnail"></img>
                            </Card.Text>
                                </div> 
                            </div>  
                        </Card.Body>
                    </Card>
                </div>
                {/* RIINNA BUNI, texto Izquierda */}
            <div className="col-md-12 mt-5" data-aos="fade-up" data-aos-offset="190">
                    <Card className="w-100">
                        <Card.Header>Rinna Bruni</Card.Header>
                        <Card.Body> 
                            <div className="row">
                            <div className="col-md-10 order-1 order-md-0">
                            <Card.Text className="myTech">
                            
                            <strong>Rinna Bruni</strong> es un referente en el mundo de la moda mexicana y una importante empresa de venta directa.
                        Con <strong>Rinna Bruni</strong> tiene todo
                        lo que buscas para ti y tu familia: originalidad, seriedad, pasión, gratitud, compromiso e innovación en todos los lanzamientos
                        que nos ofrece todas las temporadas, además debemos resaltar que las prendas se ofrecen a precios increíbles.
                                        
                            </Card.Text>
                                </div> 
                            <div className="col-md-2 order-0 order-md-1">
                            <Card.Text>
                            <img src={Rinna} alt="Rinna Catálogo" className="img-fluid w-100 img-thumbnail"></img>
                            </Card.Text>
                                </div> 
                            </div>  
                        </Card.Body>
                    </Card>
                </div>
                {/* TERRA, texto Izquierda */}
                <div className="col-md-12 mt-5 mb-5" data-aos="fade-up" data-aos-offset="190">
                    <Card className="w-100">
                        <Card.Header>Terra</Card.Header>
                        <Card.Body> 
                            <div className="row">
                            <div className="col-md-10 order-1 order-md-1">
                            <Card.Text className="myTech">
                            <strong>Terra</strong> es una empresa que cuenta con más de 25 años de experiencia en la industria ofreciendo productos de la mejor
                        calidad con las últimas tendencias de la moda adaptadas a productos accesibles para todo tipo de actividades cotidianas y sociales.
                        Su visión principal es ser líder en la comercialización de productos y servicios a través de un modelo de negocio e innovadoras herramientas
                        que impulsa el desarrollo de emprendedores independientes para generar atractivos ingresos extras.
                            </Card.Text>
                                </div> 
                            <div className="col-md-2 order-0 order-md-0">
                            <Card.Text>
                            <img src={Terra} alt="Terra Catálogo" className="img-fluid w-100 img-thumbnail"></img>
                            </Card.Text>
                                </div> 
                            </div>  
                        </Card.Body>
                    </Card>
                </div>
        </div>
    )
}

export default Inicio;

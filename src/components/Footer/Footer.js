import React from 'react';
import './footer.css'
const Footer = () => {
  return (
    <>
      <div className="main-footer">
        <div className="container">
          <div className="row">
            {/* Column1 */}
            <div className="col">
              <h4>Ubicación</h4>
              <h6 className="list-unstyled">
                {/*               <li>618-210-4853</li>
 */}              <li>Alberto Terrones #300 Benites Sur</li>
                <li>Durango, Dgo.</li>
              </h6>
            </div>
            {/* Column2 */}
            <div className="col">
              <h4>Enlaces</h4>
              <ul className="list-unstyled">
                <li><a style={{color:'white'}} href='https://multi-marcas.com/nosotros'>Acerca de nosotros</a></li>
                <li><a style={{color:'white'}} href='https://multi-marcas.com/contacto'>Contacto</a></li>
                <li><a style={{color:'white'}} href='https://multi-marcas.com/todos'>Catálogos</a></li>

              </ul>
            </div>
            {/* Column3 */}
            <div className="col">
              <h4>Redes Sociales</h4>
              <ul className="list-unstyled">
                <li><i className="fab fa-facebook"><a style={{ color: 'white', }} href={'https://www.facebook.com/Multimarcas-Durango-ventas-por-catalogo-104274345252010/'}> Facebook</a></i></li>
              </ul>
              <h4>Avisos</h4>
              <ul className="list-unstyled">
                <li><a style={{ color: 'white', }} href={'https://multi-marcas.com/terminos-y-condiciones'}> Términos y Condiciones</a></li>
                <li><a style={{ color: 'white', }} href={'https://multi-marcas.com/politicas-de-privacidad'}> Políticas de Privacidad</a></li>
              </ul>
            </div>
          </div>
          <hr />
          <div className="row">
            <p className="col-sm">
              &copy;{new Date().getFullYear()} MULTI-MARCAS.COM
            </p>
          </div>
        </div>
      </div>


    </>
  )
}

export default Footer
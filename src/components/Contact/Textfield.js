import React from 'react';
import { ErrorMessage, useField, Field } from 'formik';

export const TextField = ({ label, ...props }) => {
  const [field, meta] = useField(props);
  return (
    <div className={`mb-2 ${(props.sm===1)? 'col-md-6' : 'col-md-12'}`}>
      {props.message===1?
      (
        <div>
        <label htmlFor={field.name}>{label}</label>
      <Field as="textarea"
        className={`form-control shadow-none ${meta.touched && meta.error && 'is-invalid'}`}
        {...field} {...props}
        autoComplete="off"
      />
      </div>
      ):
      (
        <div>
          <label htmlFor={field.name}>{label}</label>
      <input
        className={`form-control shadow-none ${meta.touched && meta.error && 'is-invalid'}`}
        {...field} {...props}
        autoComplete="off"
      />
        </div>
      )}
      <ErrorMessage component="div" name={field.name} className="error" />
    </div>
  )
}
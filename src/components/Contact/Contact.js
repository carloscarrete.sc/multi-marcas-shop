import React from 'react';

import { Formik, Form, Field } from 'formik';
import * as Yup from 'yup';
import { TextField } from './Textfield';

import {addMessage} from '../../utils/message'
import { toast } from 'react-toastify';

import Spinner from 'react-bootstrap/Spinner'
import { Button } from 'react-bootstrap';

const Contact = () => {
    const validate = Yup.object({
        Nombre: Yup.string()
            .max(20, 'El nombre no debe de pasar de 15 carácteres')
            .required('El campo de Nombre es obligatorio'),
        Correo: Yup.string()
            .email('El correo no tiene un formato válido')
            .required('El campo de Correo es obligatorio'),
        Motivo: Yup.string()
            .required('El campo de Motivo es un campo obligatorio'),
        Texto: Yup.string()
            .max(2000, 'El texto no debe de pasar de 2000 carácteres')
            .required('El campo de Texto es obligatorio')
    })
    return (
        <Formik
            validationSchema={validate}
            initialValues={{
                Nombre: '',
                Correo: '',
                Motivo: '',
                Texto: ''
            }}
            onSubmit={async (values,{setSubmitting, resetForm})=>{
                if(values.Motivo!=='Seleccione una opción'){
                    setSubmitting(true)
                       await addMessage(
                       values.Nombre,
                       values.Correo,
                       values.Motivo,
                       values.Texto
                       ) 
                       resetForm();
                }else{
                    toast.warn('Para poder mandar el mensaje, debe de seleccionar un motivo',{
                        theme: 'colored'
                    })
                }

            }}
        >
            {formik => (
                <div>
                    <h1 className="d-flex justify-content-center my-4 font-weight-bold .display-4">¡Contactanos!</h1>
                    <h6 className="d-flex justify-content-center">A continuación podrás encontrar un formulario en el cual podrás ponerte en contacto con nosotros, 
                    solamente deberás de dejar tu nombre, tu correo electrónico, el motivo por el cuál te comunicas con 
                    nosotros y el mensaje que nos deseas dejar, nosotros intentaremos comunicarnos contigo a la brevedad.
                    </h6>
                    <Form>
                        <TextField label="Nombre" name="Nombre" type="text" />
                        <TextField label="Correo electrónico" name="Correo" type="text" />

{/*                          <TextField label="Motivo" name="Motivo" type="text" /> 
 */}
                        <div className="mb-2">
                        <label htmlFor="Motivo">Motivo</label>
                         <Field label="Motivo" className="form-control shadow-none w-100" as="select" name="Motivo">
                            <option value="default">Seleccione una opción</option>
                            <option value="Cotizaciones">Cotización</option>
                            <option value="Informes">Informes</option>
                            <option value="Envios">Envios</option>
                            <option value="Preguntas">Preguntas</option>
                            <option value="Afiliaciones">Afiliaciones</option>
                        </Field> 
                        </div>

                        <TextField label="Texto" name="Texto" type="text" message={1} />
                        <div className="d-flex justify-content-center">
                        <Button className="btn btn-dark m-3" type="submit" disabled={formik.isSubmitting?true:false}>
                        {formik.isSubmitting&&(
                            <Spinner
                            as="span"
                            animation="grow"
                            size="sm"
                            role="status"
                            aria-hidden="true"
                        />
                        )}
                        Enviar mi información</Button>
                            <Button className="btn btn-danger m-3" type="reset">Limpiar formulario</Button>
                        </div>
                    </Form>
                </div>
            )}
        </Formik>
    )
}

export default Contact

//React Modules
import React from 'react';
import ReactDOM from 'react-dom';
//Stylesheet
import './App.css'
import './index.css';
import 'bootstrap/dist/css/bootstrap.min.css';
//Local Imports
import App from './App'
import Footer from './components/Footer/Footer';

ReactDOM.render(
  <>
    <App />
    <Footer />
  </>,
  document.getElementById('root')
);

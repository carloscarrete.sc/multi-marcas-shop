import {BASE_PATH} from './constants'
import axios from 'axios';


export async function getProducts(catalogoActual){
    try{
        const url = `${BASE_PATH}/productos?_limit=-1&Linea=${catalogoActual}`
        const result = axios.get(url)
        return result
    }catch(error){
        console.log(error)
        return null;
    }
}
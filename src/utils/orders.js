import {BASE_PATH} from './constants'
import { authFetch } from './fetch'


export async function getOrdersByApi(idUser, logout){
    try{
        const url = `${BASE_PATH}/ordenes?_sort=created_at:desc&usuario.id=${idUser}`;
        const result = authFetch(url,null,logout);
        return result;
    }catch(error){
        console.log(error)
        return null;
    }
}


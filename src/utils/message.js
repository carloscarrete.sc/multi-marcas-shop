import {BASE_PATH} from './constants'
import axios from 'axios'

import {toast} from 'react-toastify'

export async function addMessage(nombre, correo, motivo, texto) {
    try{
        const url = `${BASE_PATH}/mensajes`;
        axios
            .post(url,{
                Nombre: nombre,
                Correo: correo,
                Motivo: motivo,
                Texto: texto
            })
            .then(response=>{
                toast.success('Se ha enviado correctamente su información',{
                    theme: 'colored'
                })
            })
            .catch(error=>{
                console.log(error);
                return null;
            })
            
    }catch(error){
        toast.error('Algo salio mal, no se pudo enviar la información',{
            theme: 'colored'
        })
        console.log(error)
        return null;
    }
}
import {BASE_PATH, CART} from './constants'
import { toast } from 'react-toastify';
import {authFetch} from './fetch'

export function getProductsCart(){
    const cart = localStorage.getItem(CART);
    if(!cart){
        return null
    }else{
        const products = cart.split(",");
        return products;
    }
}


export function addProductCart(product){
    const cart = getProductsCart();
    if(!cart){
        localStorage.setItem(CART,product);
        toast.success('El producto ha sido agregado al carrito', {
            theme: 'colored'
        })
    }else{
        const productFound = cart.includes(product);
        if(productFound){
            toast.info('Este producto ya esta en el carrito',{
                theme: 'colored'
            })
        }else{
            cart.push(product);
            localStorage.setItem(CART, cart)
            toast.success('El producto ha sido agregado al carrito', {
                theme: 'colored'
            })
        }
    }
}


export function countProductsCart(){
    const cart = getProductsCart();
    if(!cart){
        return 0
    }else{
        return cart.length;
    }
}

export function removeProductCart(product){
    const cart = getProductsCart();
    const indexOfProduct = cart.indexOf(product)
    cart.splice(indexOfProduct,1)

    if(cart.length>0){
        localStorage.setItem(CART,cart);
    }else{
        localStorage.removeItem(CART)
    } 
}

export async function paymentCardApi(token, products,idUser, email, address, shipping, logout){
    try{    
        const addressShipping = address;
        delete addressShipping.user;
        delete addressShipping.createAt;

        const url = `${BASE_PATH}/ordenes`;
        const params = {
            method: 'POST',
            headers: {
                "Content-Type" : "application/json"
            },
            body: JSON.stringify({
                token,
                products,
                idUser,
                addressShipping,
                shipping,
                email
            }) 

        }
        const result = await authFetch(url, params, logout);
        return result;
    }catch(error){
        console.log(error)
        return null;
    }
}

export function removeAllCartProducts(){
    localStorage.removeItem(CART);
}
import { authFetch } from "./fetch";
import { toast } from 'react-toastify';

import { BASE_PATH } from "./constants";

export async function crearDireccionNueva(address,logout){
    try{
        const url = `${BASE_PATH}/direcciones`;
        const params = {
            method: "POST",
            headers: {
                "Content-Type" : "application/json",
            },
            body: JSON.stringify(address),
        };
        const result = await authFetch(url, params, logout);
        toast.success('La dirección ha sido creada',{
            theme: "colored"
        })

        return result;
    }catch(error){
        console.log(error);
        toast.warning('No se ha podido crear la dirección', {
            theme: "colored"
        })
        return null
    }
}

export async function getAdress(idUser, logout){
    try{
        const url = `${BASE_PATH}/direcciones?user=${idUser}`;
        const result = await authFetch(url,null,logout);
        //if(result.statusCode===500) throw 'Error del servidor';
        return result;
    }catch(error){
        console.log(error)
        return null;
    }
}

export async function deleteAddressFromUser(idAddress, logout){
    try{
        const url = `${BASE_PATH}/direcciones/${idAddress}`
        const params = {
            method: 'DELETE',
            headers: {
                "Content-Type" : "application/json",
            },
        };
        const result = await authFetch(url, params, logout);
        toast.info('La dirección ha sido eliminada', {
            theme: "colored"
        })
        if(result.statusCode===500) throw new Error();
        return true;
    }catch(error){
        console.log(error)
        toast.warning('No se ha podido eliminar la dirección',{
            theme: "colored"
        })
        return false;
    }
}

export async function getSingleAdress(idAddress, logout){
    try{
        const url = `${BASE_PATH}/direcciones/${idAddress}`
        const result = await authFetch(url,null,logout);
        return result;
    }catch(error){
        console.log(error)
        return false
    }
}


export async function updateAddress(idAddress,address,logout){
    try{
        const url = `${BASE_PATH}/direcciones/${idAddress}`
        const params = {
            method: 'PUT',
            headers: {
                "Content-Type" : "application/json",
            },
            body: JSON.stringify(address),
        };
        const result  = await authFetch(url, params, logout)
        toast.success('La dirección se ha actualizado correctamente',{
            theme: "colored"
        })
        return result;
    }catch(error){
        console.log(error)
        toast.warning('No se ha podido actualizar la dirección',{
            theme: "colored"
        })
        return null;
    }
}
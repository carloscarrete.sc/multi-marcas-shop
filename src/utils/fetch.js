import {getToken, hasExpiredToken} from './token';

export async function authFetch(url, params, logout){
    const token = getToken();
    if(!token){
        //El usuario no esta logueado
        logout();
    }else{
        if(hasExpiredToken(token)){
            //El token ha caducado
            logout()
        }else{
            const paramsTemp = {
                ...params,
                headers: {
                    ...params?.headers,
                    Authorization: `Bearer ${token}`,
                }
            }
            //console.log(paramsTemp)
            try{
                const response = await fetch(url, paramsTemp);
                const result = await response.json()
                return result;
            }catch(error){
                console.log(error)
                return error
            }
        };
    }
}

import {BASE_PATH} from './constants'

export async function getProductByName(sku){
    const url = `${BASE_PATH}/productos?SKU=${sku}`
    try{
        const response = await fetch(url)
        const result = await response.json()
        return result
    }catch(error){
        console.log(error)
    }
}